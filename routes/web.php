<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');
Route::get('login', 'HomeController@login');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('privacy', 'HomeController@privacy');



Route::group([
    'prefix' => 'admin',
    'namespace'  => 'Admin',
], function () {
    Route::get('/', 'HomeController@index');
    Route::post('notification/send', 'HomeController@sendNotification');
    Route::get('login', ['uses' => 'AuthController@index','as'=>'getlogin']);
    Route::post('login', [
        'uses' => 'AuthController@login',
        'as' => 'login',
    ]);
    Route::group([
        'middleware' => 'auth',
    ], function() {
        Route::post('logout', [
            'uses' => 'AuthController@logout',
            'as' => 'logout',
        ]);


//  //  //  //  //  //  //  //  //  //  //

        Route::group([
            'prefix'  => 'admins'
        ], function() {
            Route::get('/', 'AdminController@index');
            Route::get('update/{id}', 'AdminController@getUpdate');
            Route::get('create', 'AdminController@getCreate');
            Route::post('update/{id}', 'AdminController@postUpdate');
            Route::post('create', 'AdminController@postCreate');
            Route::post('update-password', 'AdminController@updatePassword');
            Route::post('destroy', 'AdminController@destroy');
        });
        Route::group([
            'prefix'  => 'users'
        ], function() {
            Route::get('/', 'UserController@index');
            Route::get('update/{id}', 'UserController@getUpdate');
            Route::get('create', 'UserController@getCreate');
            Route::post('update/{id}', 'UserController@postUpdate');
            Route::post('create', 'UserController@postCreate');
            Route::post('update-password', 'UserController@updatePassword');
            Route::post('destroy', 'UserController@destroy');
        });
        Route::group([
            'prefix'  => 'categories'
        ], function() {
            Route::get('/', 'CategoryController@index');
            Route::get('update/{id}', 'CategoryController@getUpdate');
            Route::get('create', 'CategoryController@getCreate');
            Route::post('update/{id}', 'CategoryController@postUpdate');
            Route::post('create', 'CategoryController@postCreate');
            Route::post('destroy', 'CategoryController@destroy');
            Route::group([
                'prefix'  => 'sub/{id}'
            ], function() {
                Route::get('/', 'SubCategoryController@index');
                Route::get('update/{id2}', 'SubCategoryController@getUpdate');
                Route::get('create', 'SubCategoryController@getCreate');
                Route::post('update/{id2}', 'SubCategoryController@postUpdate');
                Route::post('create', 'SubCategoryController@postCreate');
                Route::post('destroy', 'SubCategoryController@destroy');
            });
        });
        Route::group([
            'prefix'  => 'text_categories'
        ], function() {
            Route::get('/', 'TextCategoryController@index');
            Route::get('update/{id}', 'TextCategoryController@getUpdate');
            Route::get('create', 'TextCategoryController@getCreate');
            Route::post('update/{id}', 'TextCategoryController@postUpdate');
            Route::post('create', 'TextCategoryController@postCreate');
            Route::post('destroy', 'TextCategoryController@destroy');
            Route::group([
                'prefix'  => 'text/{id}'
            ], function() {
                Route::get('/', 'TextDesignController@index');
                Route::get('update/{id2}', 'TextDesignController@getUpdate');
                Route::get('create', 'TextDesignController@getCreate');
                Route::post('update/{id2}', 'TextDesignController@postUpdate');
                Route::post('create', 'TextDesignController@postCreate');
                Route::post('destroy', 'TextDesignController@destroy');
            });
        });

        Route::group([
            'prefix'  => 'products'
        ], function() {
            Route::get('/', 'ProductController@index');
            Route::get('update/{id}', 'ProductController@getUpdate');
            Route::get('create', 'ProductController@getCreate');
            Route::post('update/{id}', 'ProductController@postUpdate');
            Route::post('create', 'ProductController@postCreate');
            Route::post('destroy', 'ProductController@destroy');
            Route::post('delete_img/{id}', 'ProductController@delete_img');
            Route::get('end/{id}', 'ProductController@end');

        });
        Route::group([
            'prefix'  => 'delivery_options'
        ], function() {
            Route::get('/', 'DeliveryOptionController@index');
            Route::get('update/{id}', 'DeliveryOptionController@getUpdate');
            Route::get('create', 'DeliveryOptionController@getCreate');
            Route::post('update/{id}', 'DeliveryOptionController@postUpdate');
            Route::post('create', 'DeliveryOptionController@postCreate');
            Route::post('destroy', 'DeliveryOptionController@destroy');
        });
        Route::group([
            'prefix'  => 'promo_codes'
        ], function() {
            Route::get('/', 'PromoCodeController@index');
            Route::get('update/{id}', 'PromoCodeController@getUpdate');
            Route::get('create', 'PromoCodeController@getCreate');
            Route::post('update/{id}', 'PromoCodeController@postUpdate');
            Route::post('create', 'PromoCodeController@postCreate');
            Route::post('destroy', 'PromoCodeController@destroy');
        });
        Route::group([
            'prefix'  => 'advertisements'
        ], function() {
            Route::get('/', 'AdvertisementController@index');
            Route::get('update/{id}', 'AdvertisementController@getUpdate');
            Route::get('create', 'AdvertisementController@getCreate');
            Route::post('update/{id}', 'AdvertisementController@postUpdate');
            Route::post('create', 'AdvertisementController@postCreate');
            Route::post('destroy', 'AdvertisementController@destroy');
        });
        Route::group([
            'prefix'  => 'bank_accounts'
        ], function() {
            Route::get('/', 'BankAccountController@index');
            Route::get('update/{id}', 'BankAccountController@getUpdate');
            Route::get('create', 'BankAccountController@getCreate');
            Route::post('update/{id}', 'BankAccountController@postUpdate');
            Route::post('create', 'BankAccountController@postCreate');
            Route::post('destroy', 'BankAccountController@destroy');
        });
        Route::group([
            'prefix'  => 'orders'
        ], function() {
            Route::get('/', 'OrderController@index');
            Route::get('/{id}', 'OrderController@show');
            Route::get('end/{id}', 'OrderController@end');
            Route::post('cancel', 'OrderController@cancel');
            Route::get('print/{id}', 'OrderController@print');
        });
        Route::group([
            'prefix'  => 'settings'
        ], function() {
            Route::get('/', 'SettingController@index');
            Route::get('update/{id}', 'SettingController@getUpdate');
            Route::post('update/{id}', 'SettingController@postUpdate');
        });
        Route::group([
            'prefix'  => 'gift_cards'
        ], function() {
            Route::post('/destroy', 'GiftCardController@destroy');
            Route::get('/' ,'GiftCardController@index');
            Route::post('/' ,'GiftCardController@store');
            Route::get('/create' ,'GiftCardController@create');
            Route::get('edit/{id}', 'GiftCardController@edit');
            Route::post('update/{id}', 'GiftCardController@update');

        });
        Route::group([
            'prefix'  => 'luxury_packaging'
        ], function() {
            Route::post('/destroy', 'LuxuryPackagingController@destroy');
            Route::get('/' ,'LuxuryPackagingController@index');
            Route::post('/' ,'LuxuryPackagingController@store');
            Route::get('/create' ,'LuxuryPackagingController@create');
            Route::get('edit/{id}', 'LuxuryPackagingController@edit');
            Route::post('update/{id}', 'LuxuryPackagingController@update');

        });
        Route::group([
            'prefix'=>'tickets'
        ],function () {
            Route::get('/','TicketController@index');
            Route::get('/create','TicketController@create');
            Route::post('/','TicketController@store');
            Route::get('/{ticket}','TicketController@show');
            Route::post('/{ticket}/response','TicketController@response');
            Route::get('/{ticket}/close','TicketController@close');
        });
    });
});
