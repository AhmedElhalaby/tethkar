<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::group([
    'prefix' => 'auth',
    'namespace'  => 'Api',
], function () {
    Route::post('login', 'AuthController@login');
    Route::post('signup', 'AuthController@register');
    Route::post('forget_password', 'AuthController@forget_password');
    Route::post('reset_password', 'AuthController@reset_password');
    Route::group([
        'middleware' => 'auth:api'
    ], function() {
        Route::get('me', 'AuthController@show');
        Route::post('refresh', 'AuthController@refresh');
        Route::post('update', 'AuthController@update');
        Route::post('change_password', 'AuthController@change_password');
        Route::post('logout', 'AuthController@logout');
    });
});

Route::group([
    'namespace'  => 'Api',
], function () {
    Route::get('install', 'HomeController@index');
    Route::get('products', 'ProductController@index');
    Route::get('product/{id}', 'ProductController@show');
    Route::get('order/{id}', 'OrderController@show');
    Route::post('new_v_order', 'OrderController@v_store');
    Route::get('check_code', 'OrderController@check');

    Route::group([
        'middleware' => 'auth:api'
    ], function() {
        Route::post('new_order', 'OrderController@store');
        Route::post('new_order_arr', 'OrderController@store_arr');
        Route::get('orders', 'OrderController@index');

        Route::get('notifications', 'NotificationController@index');
        Route::post('read_all', 'NotificationController@read_all');
        Route::post('read/{id}', 'NotificationController@update');
        Route::post('orders/cancel/{id}', 'OrderController@cancel');
        Route::get('category/{id}', 'TicketController@category');
        Route::group([
            'prefix' => 'transactions',
        ], function() {
            Route::get('/', 'TransactionController@index');
            Route::get('my_balance', 'TransactionController@my_balance');
            Route::post('generate_checkout', 'TransactionController@generate_checkout');
            Route::get('check_payment', 'TransactionController@check_payment');
        });

        Route::group([
            'prefix' => 'tickets',
        ], function() {
            Route::get('/','TicketController@index');
            Route::post('store','TicketController@store');
            Route::get('show','TicketController@show');
            Route::post('response','TicketController@response');
        });

    });
});
