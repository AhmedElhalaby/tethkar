<?php

use Illuminate\Http\Request;
//use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
| Here is where you can register Admin routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "admin" middleware group. Enjoy building your Admin panel!
|
*/


//
//Route::group([
//    'namespace'  => 'Admin',
//], function() {
//    Route::get('login', ['uses' => 'AuthController@index']);
//    Route::post('login', [
//        'uses' => 'AuthController@login',
//        'as' => 'login',
//    ]);
//    Route::group([
//        'middleware' => 'auth:admin',
//    ], function() {
//        Route::post('logout', [
//            'uses' => 'AuthController@logout',
//            'as' => 'logout',
//        ]);
//    });
//});
