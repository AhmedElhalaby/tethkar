@extends('admin.layouts.app')
@section('style')
    <style>
        .TicketCard{
            padding: 10px 25px;
            border-radius: 20px;
            margin: 20px;
            font-size: 17px;
            min-width: 250px;
            display: inline-block;
        }
        .TicketCardUser{
            background-color: #e2e0e0;
        }
        .TicketCardAdmin{
            background-color: #e3b9b9;
        }

    </style>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header " data-background-color="{{ config('app.color') }}">
                    <h4 class="title" style="display: inline-block">التذكرة </h4>
                    @if($Object->status == \App\Helpers\Constant::TICKETS_STATUS['Open'])
                        <a href="{{url('admin/tickets/'.$Object->id.'/close')}}" class="btn btn-white" style="margin: 0;float: left;color: #f4511e "><i class="fa fa-window-close"></i>   اغلاق</a>
                    @endif

                </div>
                <div class="card-content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-content table-responsive">
                                    <div class="col-md-12">
                                        <div class="TicketCard TicketCardUser">
                                            <strong> {{$Object->title}}</strong>
                                            <br>
                                            <span>{{$Object->message}}</span>
                                        </div>
                                    </div>

                                    @foreach($Object->ticket_responses as $response)
                                        <div class="col-md-12  @if($response->sender_type == \App\Helpers\Constant::SENDER_TYPE['User']) TicketCardLRUser @else TicketCardLRAdmin @endif">
                                            <div class=" TicketCard @if($response->sender_type == \App\Helpers\Constant::SENDER_TYPE['User']) TicketCardUser @else TicketCardAdmin @endif">
                                                <span>{{$response->response}}</span>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        @if($Object->status== \App\Helpers\Constant::TICKETS_STATUS['Open'] && $Object->user_id != null)
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-content table-responsive">
                                    <form action="{{url('admin/tickets'.'/'.$Object->id.'/response')}}" method="post">
                                        @csrf
                                        <div class="row">
                                            <div class="col-md-12">
                                                <p>رسالة الرد</p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group label-floating">
                                                    <label for="ticket_response" class="control-label">رسالة *</label>
                                                    <textarea id="ticket_response" name="ticket_response" required class="form-control {{ $errors->has('ticket_response') ? ' is-invalid' : '' }}"></textarea>
                                                </div>
                                                @if ($errors->has('ticket_response'))
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('ticket_response') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="row submit-btn">
                                            <button type="submit" class="btn btn-primary" style="margin-left:15px;margin-right:15px;">ارسال</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
