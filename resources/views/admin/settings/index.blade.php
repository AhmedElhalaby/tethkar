@extends('admin.layouts.app')
@section('style')
    <style>
        .pagination>.active>a, .pagination>.active>a:focus, .pagination>.active>a:hover, .pagination>.active>span, .pagination>.active>span:focus, .pagination>.active>span:hover {
            background-color: #9c27b0;
            border-color: #9c27b0;
        }
        .pagination>li>a, .pagination>li>span {
            color: #9c27b0;
        }
        .pagination>li>a:focus, .pagination>li>a:hover, .pagination>li>span:focus, .pagination>li>span:hover {
            color: #9c27b0;
            box-shadow: 0 1px 4px 0 rgba(0, 0, 0, 0.14);
        }
        .pagination {
            box-shadow: 0 1px 4px 0 rgba(0, 0, 0, 0.14);
        }
        .pagination-div{
            margin-left: 15px;
            margin-right: 15px;
            float: left;
        }
    </style>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header" data-background-color="{{ config('app.color') }}">
                    <h4 class="title">الإعدادات</h4>
                    <p class="category">هنا بيانات كل الإعدادات </p>
                </div>
                <div class="card-content table-responsive">
                    <table class="table">
                        <thead class="text-primary">
                            <th>الاعداد</th>
                            <th>القيمة</th>
                            <th></th>
                        </thead>
                        <tbody>
                        @foreach($Settings as $item)
                        <tr>
                            <td>{{$item->key}}</td>
                            <td>{{$item->value}}</td>
                            <td class="text-primary">
                                <a href="{{url('admin/settings/update/'.$item->id)}}" data-toggle="tooltip" data-placement="bottom" title="تعديل" class="fs-20"><i class="fa fa-edit"></i></a>
                            </td>
                        </tr>
                        <!-- Modal -->
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="pagination-div">
            {{ $Settings->links() }}
        </div>
    </div>
@endsection
