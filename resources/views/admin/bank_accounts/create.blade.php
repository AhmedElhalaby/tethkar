@extends('admin.layouts.app')
@section('style')
    <style>
        .pagination>.active>a, .pagination>.active>a:focus, .pagination>.active>a:hover, .pagination>.active>span, .pagination>.active>span:focus, .pagination>.active>span:hover {
            background-color: #9c27b0;
            border-color: #9c27b0;
        }
        .pagination>li>a, .pagination>li>span {
            color: #9c27b0;
        }
        .pagination>li>a:focus, .pagination>li>a:hover, .pagination>li>span:focus, .pagination>li>span:hover {
            color: #9c27b0;
            box-shadow: 0 1px 4px 0 rgba(0, 0, 0, 0.14);
        }
        .pagination {
            box-shadow: 0 1px 4px 0 rgba(0, 0, 0, 0.14);
        }
        .pagination-div{
            margin-left: 15px;
            margin-right: 15px;
            float: left;
        }
    </style>
@endsection
@section('head-icon')
    <a href="{{url('admin/bank_accounts/create')}}" style="display: inline-block;">
        <i class="material-icons text-primary" data-toggle="tooltip" data-placement="bottom" title="إضافة حساب بنكي جديد" style="font-size: 30px">add_box</i>
        <p class="hidden-lg hidden-md">Dashboard</p>
    </a>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header" data-background-color="{{ config('app.color') }}">
                    <h4 class="title">إضافة حساب بنكي جديد</h4>
                    <p class="category">إضافة حساب بنكي جديد</p>
                </div>
                <div class="card-content">
                    <form action="{{url('admin/bank_accounts/create')}}" method="post" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group label-floating">
                                    <label for="bank_name" class="control-label">اسم البنك انجليزي *</label>
                                    <input type="text" id="bank_name" name="bank_name" required class="form-control {{ $errors->has('bank_name') ? ' is-invalid' : '' }}" value="{{old('bank_name')}}">
                                </div>
                                @if ($errors->has('bank_name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('bank_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-6">
                                <div class="form-group label-floating">
                                    <label for="bank_name_ar" class="control-label">اسم البنك عربي *</label>
                                    <input type="text" id="bank_name_ar" name="bank_name_ar" required class="form-control {{ $errors->has('bank_name_ar') ? ' is-invalid' : '' }}" value="{{old('bank_name_ar')}}">
                                </div>
                                @if ($errors->has('bank_name_ar'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('bank_name_ar') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group label-floating">
                                    <label for="account_num" class="control-label">رقم الحساب *</label>
                                    <input type="text" id="account_num" name="account_num" required class="form-control {{ $errors->has('account_num') ? ' is-invalid' : '' }}" value="{{old('account_num')}}">
                                </div>
                                @if ($errors->has('account_num'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('account_num') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <button type="submit" class="btn btn-primary" style="float: left;margin-left:15px;margin-right:15px;">حفظ</button>
                        </div>
                        <div class="clearfix"></div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('script')
    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah')
                        .attr('src', e.target.result);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
@endsection
