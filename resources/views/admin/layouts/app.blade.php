
<!DOCTYPE html>
<html lang="en" data-color="{{ config('app.color') }}">
<head>
    <meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="{{asset('public/assets/img/apple-icon.png')}}" />
    <link rel="icon" type="image/png" href="{{asset('public/assets/img/favicon.png')}}" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name') }}</title>


    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />

    <!-- Bootstrap core CSS     -->
    <link href="{{asset('public/assets/css/bootstrap.min.css')}}" rel="stylesheet" />

    <!-- Bootstrap rtl CSS - from (http://github.com/morteza) -->
    <link rel="stylesheet" href="{{asset('public/assets/css/bootstrap-rtl.min.css')}}">

    <!--  Material Dashboard CSS    -->
    <link href="{{asset('public/assets/css/material-dashboard.css')}}" rel="stylesheet"/>

    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="{{asset('public/assets/css/demo.css')}}" rel="stylesheet" />

    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300|Material+Icons' rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Cairo" rel="stylesheet">
    <link href="{{asset('public/assets/css/custom.css')}}" rel="stylesheet" />
    @yield('style')
</head>

<body>

<div class="wrapper">

    <div class="sidebar" data-color="{{ config('app.color') }}" data-image="{{asset('public/assets/img/sidebar-1.jpg')}}">
        <!--
            Tip 1: You can change the color of the sidebar using: data-color="purple | blue | green | orange | red"

            Tip 2: you can also add an image using data-image tag
        -->

        <div class="logo">
            <a href="{{url('admin')}}" class="simple-text">
                {{ config('app.name') }}
            </a>
        </div>

        <div class="sidebar-wrapper">
            <ul class="nav">
                <li @if(url()->current() == url('admin')) class="active" @endif>
                    <a href="{{url('admin')}}">
                        <i class="material-icons">dashboard</i>
                        <p>الرئيسية</p>
                    </a>
                </li>
                <li @if(url()->current() == url('admin/orders')) class="active" @endif>
                    <a href="{{url('admin/orders')}}">
                        <i class="material-icons">shopping_cart</i>
                        <p>الطلبات</p>
                    </a>
                </li>
                <li @if(url()->current() == url('admin/users')) class="active" @endif>
                    <a href="{{url('admin/users')}}">
                        <i class="material-icons">person</i>
                        <p>المستخدمين</p>
                    </a>
                </li>
                <li @if(url()->current() == url('admin/admins')) class="active" @endif>
                    <a href="{{url('admin/admins')}}">
                        <i class="material-icons">settings</i>
                        <p>المدراء</p>
                    </a>
                </li>
                <li @if(url()->current() == url('admin/categories')) class="active" @endif>
                    <a href="{{url('admin/categories')}}">
                        <i class="material-icons">category</i>
                        <p>التصنيفات</p>
                    </a>
                </li>
                <li @if(url()->current() == url('admin/text_categories')) class="active" @endif>
                    <a href="{{url('admin/text_categories')}}">
                        <i class="material-icons">description</i>
                        <p>النصوص المقترحة</p>
                    </a>
                </li>
                <li @if(url()->current() == url('admin/products')) class="active" @endif>
                    <a href="{{url('admin/products')}}">
                        <i class="material-icons">list_alt</i>
                        <p>المنتجات</p>
                    </a>
                </li>
                <li @if(url()->current() == url('admin/delivery_options')) class="active" @endif>
                    <a href="{{url('admin/delivery_options')}}">
                        <i class="material-icons">directions_transit</i>
                        <p>طرق التوصيل</p>
                    </a>
                </li>
                <li @if(url()->current() == url('admin/tickets')) class="active" @endif>
                    <a href="{{url('admin/tickets')}}">
                        <i class="material-icons">label</i>
                        <p>  التذكرة</p>
                    </a>
                </li>

                <li @if(url()->current() == url('admin/promo_codes')) class="active" @endif>
                    <a href="{{url('admin/promo_codes')}}">
                        <i class="material-icons">tag</i>
                        <p>أكواد الخصم</p>
                    </a>
                </li>
                <li @if(url()->current() == url('admin/luxury_packaging')) class="active" @endif>
                    <a href="{{url('admin/luxury_packaging')}}">
                        <i class="material-icons">star</i>
                        <p> تغليف فاخر</p>
                    </a>
                </li>
                <li @if(url()->current() == url('admin/gift_cards')) class="active" @endif>
                    <a href="{{url('admin/gift_cards')}}">
                        <i class="material-icons">font_download</i>
                        <p> بطاقة اهداء</p>
                    </a>
                </li>

                <li @if(url()->current() == url('admin/bank_accounts')) class="active" @endif>
                    <a href="{{url('admin/bank_accounts')}}">
                        <i class="material-icons">credit_card</i>
                        <p>الحسابات البنكية</p>
                    </a>
                </li>

                <li @if(url()->current() == url('admin/advertisements')) class="active" @endif>
                    <a href="{{url('admin/advertisements')}}">
                        <i class="material-icons">font_download</i>
                        <p>الإعلانات</p>
                    </a>
                </li>
                <li @if(url()->current() == url('admin/settings/')) class="active" @endif>
                    <a href="{{url('admin/settings/')}}">
                        <i class="fa fa-cogs" aria-hidden="true"></i>
                        <p>الإعدادات</p>
                    </a>
                </li>
            </ul>
        </div>
    </div>

    <div class="main-panel">
        <nav class="navbar navbar-transparent navbar-absolute">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    {{--<a class="navbar-brand" href="#"></a>--}}
                </div>
                <div class="collapse navbar-collapse" >
                    <ul class="nav navbar-nav navbar-right">
                        <li>
                            @yield('head-icon')
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                            <a href="{{route('logout')}}" style="display: inline-block;" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                                <i class="material-icons text-primary" data-toggle="tooltip" data-placement="bottom" title="تسجيل الخروج" style="font-size: 30px">logout</i>
                                <p class="hidden-lg hidden-md">logout</p>
                            </a>
                        </li>

                    </ul>
                </div>
            </div>
        </nav>

        <div class="content">
            <div class="container-fluid">
                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                @endif
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                @yield('content')

            </div>
        </div>
    </div>
</div>
@yield('out-content')

</body>

<!--   Core JS Files   -->
<script src="{{asset('public/assets/js/jquery-3.1.0.min.js')}}" type="text/javascript"></script>
<script src="{{asset('public/assets/js/bootstrap.min.js')}}" type="text/javascript"></script>
<script src="{{asset('public/assets/js/material.min.js')}}" type="text/javascript"></script>

<!--  Charts Plugin -->
<script src="{{asset('public/assets/js/chartist.min.js')}}"></script>

<!--  Notifications Plugin    -->
<script src="{{asset('public/assets/js/bootstrap-notify.js')}}"></script>

<!--  Google Maps Plugin    -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js"></script>

<!-- Material Dashboard javascript methods -->
<script src="{{asset('public/assets/js/material-dashboard.js')}}"></script>

<script src="{{asset('public/assets/js/custom.js')}}"></script>

<script>
    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    })
</script>
@yield('script')


</html>
