@extends('admin.layouts.app')
@section('style')

    <style>
        .pagination>.active>a, .pagination>.active>a:focus, .pagination>.active>a:hover, .pagination>.active>span, .pagination>.active>span:focus, .pagination>.active>span:hover {
            background-color: #9c27b0;
            border-color: #9c27b0;
        }
        .pagination>li>a, .pagination>li>span {
            color: #9c27b0;
        }
        .pagination>li>a:focus, .pagination>li>a:hover, .pagination>li>span:focus, .pagination>li>span:hover {
            color: #9c27b0;
            box-shadow: 0 1px 4px 0 rgba(0, 0, 0, 0.14);
        }
        .pagination {
            box-shadow: 0 1px 4px 0 rgba(0, 0, 0, 0.14);
        }
        .pagination-div{
            margin-left: 15px;
            margin-right: 15px;
            float: left;
        }
    </style>
@endsection
@section('head-icon')
    <a href="{{url('admin/products/create')}}" style="display: inline-block;">
        <i class="material-icons text-primary" data-toggle="tooltip" data-placement="bottom" title="إضافة منتج جديد" style="font-size: 30px">add_box</i>
        <p class="hidden-lg hidden-md">Dashboard</p>
    </a>
@endsection
@section('content')
    <form class="row" action="{{url('admin/products/create')}}" method="post" enctype="multipart/form-data">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header" data-background-color="{{ config('app.color') }}">
                    <h4 class="title">إضافة منتج جديد</h4>
                    <p class="category">إضافة منتج جديد</p>
                </div>
                <div class="card-content">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group label-floating">
                                <label for="name" class="control-label">الاسم انجليزي *</label>
                                <input type="text" id="name" name="name" required class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }}" value="{{old('name')}}">
                            </div>
                            @if ($errors->has('name'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="col-md-6">
                            <div class="form-group label-floating">
                                <label for="ar_name" class="control-label">الاسم عربي *</label>
                                <input type="text" id="ar_name" name="ar_name" required class="form-control {{ $errors->has('ar_name') ? ' is-invalid' : '' }}" value="{{old('ar_name')}}">
                            </div>
                            @if ($errors->has('ar_name'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('ar_name') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group label-floating">
                                <label for="category_id" class="control-label">التصنيف *</label>
                                <select name="category_id" id="category_id" required class="form-control {{ $errors->has('category_id') ? ' is-invalid' : '' }}">
                                    @foreach(\App\Models\Category::where('parent',0)->get() as $category)
                                        <option value="{{$category->id}}" @if(old('category_id') == $category->id) selected @endif>{{$category->name}}</option>
                                        @foreach(\App\Models\Category::where('parent',$category->id)->get() as $sub)
                                            <option style="padding-right: 10px" value="{{$sub->id}}" @if(old('category_id') == $sub->id) selected @endif>{{$sub->name}}</option>
                                        @endforeach
                                    @endforeach
                                </select>
                            </div>
                            @if ($errors->has('category_id'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('category_id') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group label-floating">
                                <label for="price" class="control-label">السعر *</label>
                                <input type="number" id="price" name="price" required class="form-control {{ $errors->has('price') ? ' is-invalid' : '' }}" value="{{old('price')}}">
                            </div>
                            @if ($errors->has('price'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('price') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="col-md-4">
                            <div class="form-group label-floating">
                                <label for="delivery_option" class="control-label">امكانية التوصيل *</label>
                                <select name="delivery_option" id="delivery_option" required class="form-control {{ $errors->has('delivery_option') ? ' is-invalid' : '' }}">
                                    <option value="1" @if(old('delivery_option') == 1) selected @endif>التوصيل مدعوم</option>
                                    <option value="0" @if(old('delivery_option') == 0) selected @endif> التوصيل غير مدعوم</option>
                                </select>
                            </div>
                            @if ($errors->has('delivery_option'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('delivery_option') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group label-floating">
                                <label for="description" class="control-label">الوصف انجليزي *</label>
                                <textarea id="description" name="description" required class="form-control {{ $errors->has('description') ? ' is-invalid' : '' }}">{{old('description')}}</textarea>
                            </div>
                            @if ($errors->has('description'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('description') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="col-md-12">
                            <div class="form-group label-floating">
                                <label for="ar_description" class="control-label">الوصف عربي *</label>
                                <textarea id="ar_description" name="ar_description" required class="form-control {{ $errors->has('ar_description') ? ' is-invalid' : '' }}">{{old('ar_description')}}</textarea>
                            </div>
                            @if ($errors->has('ar_description'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('ar_description') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="row">
                        <button type="submit" class="btn btn-primary" style="float: left;margin-left:15px;margin-right:15px;">حفظ</button>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card">
                <div class="card-header" data-background-color="{{ config('app.color') }}">
                    <h4 class="title">صور المنتج</h4>
                    <p class="category">Min width 500px - 2:1 w:h</p>
                </div>
                <div class="card-content">
                    <br>
                    <div class="card-header" data-background-color="{{ config('app.color') }}" style="cursor: pointer">
                        <h5 class="title" onclick="document.getElementById('image').click()">اضغط هنا لرفع صور المنتج</h5>

                        <div id="blah">

                        </div>
                    </div>
                    <input type="file" id="image" name="image[]" style="display: none" multiple onchange="readURL(this)">
                </div>
            </div>
        </div>
    </form>

@endsection
@section('script')
    <script>
        function readURL(input) {
            let file,reader,image_html,i,len;
            if (input.files && input.files[0]) {
                let ref = input.files;
                for (i = 0, len = ref.length; i < len; i++){
                    file = ref[i];
                    reader = new FileReader();
                    reader.onload = function (e) {
                        image_html = `<br><img src="${e.target.result}" class="thumbnail" alt="">`;
                        $('#blah').append(image_html);
                    };
                    reader.readAsDataURL(file);
                }
            }
        }
    </script>
@endsection
