@extends('admin.layouts.app')
@section('style')
    <style>
        .pagination>.active>a, .pagination>.active>a:focus, .pagination>.active>a:hover, .pagination>.active>span, .pagination>.active>span:focus, .pagination>.active>span:hover {
            background-color: #9c27b0;
            border-color: #9c27b0;
        }
        .pagination>li>a, .pagination>li>span {
            color: #9c27b0;
        }
        .pagination>li>a:focus, .pagination>li>a:hover, .pagination>li>span:focus, .pagination>li>span:hover {
            color: #9c27b0;
            box-shadow: 0 1px 4px 0 rgba(0, 0, 0, 0.14);
        }
        .pagination {
            box-shadow: 0 1px 4px 0 rgba(0, 0, 0, 0.14);
        }
        .pagination-div{
            margin-left: 15px;
            margin-right: 15px;
            float: left;
        }
    </style>
@endsection
@section('head-icon')
    <a href="{{url('admin/products/create')}}" style="display: inline-block;">
        <i class="material-icons text-primary" data-toggle="tooltip" data-placement="bottom" title="إضافة منتج جديد" style="font-size: 30px">add_box</i>
        <p class="hidden-lg hidden-md">Dashboard</p>
    </a>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header" data-background-color="{{ config('app.color') }}">
                    <h4 class="title">المنتجات</h4>
                    <p class="category">هنا بيانات كل المنتجات </p>
                </div>
                <div class="card-content table-responsive">
                    <table class="table">
                        <thead class="text-primary">
                            <th></th>
                            <th>الاسم</th>
                            <th>التصنيف</th>
                            <th>السعر</th>
                            <th>الحالة</th>
                            <th></th>
                        </thead>
                        <tbody>
                        @foreach($Products as $item)
                        <tr>
                            <td><img src="@if($item->image()){{asset($item->image()->image)}}@endif" alt="" style="width: 45px; height: 45px"></td>
                            <td>{{$item->ar_name}}</td>
                            <td><a href="{{url('admin/categories/update/'.$item->category->id)}}" class="text-primary">{{$item->category->ar_name}}</a></td>
                            <td>{{$item->price}}</td>
                            <td>
                                @if($item->status == 1)
                                    <span class="label label-success">متوفر</span>
                                @else
                                    <span class="label label-danger">الكمية منتهية</span>
                                @endif
                            </td>
                            <td class="text-primary">
                                <a href="{{url('admin/products/update/'.$item->id)}}" data-toggle="tooltip" data-placement="bottom" title="تعديل" class="fs-20"><i class="fa fa-edit"></i></a>
                                <a href="{{url('admin/products/end/'.$item->id)}}" class="fs-20" data-toggle="tooltip" data-placement="bottom" @if($item->status == 1) title="وضع ملصق نفاذ الكمية" @else title="ازالة ملصق نفاذ الكمية" @endif><i class="fa  @if($item->status == 1) fa-window-close @else fa-check @endif"></i></a>
                                <a href="#delete" class="fs-20" data-toggle="modal" data-target="#delete" onclick="document.getElementById('del_UserName').innerHTML = '{{$item->ar_name}}';document.getElementById('del_user_id').value = '{{$item->id}}'"><i class="fa fa-trash" data-toggle="tooltip" data-placement="bottom" title="حذف"></i></a>
                            </td>
                        </tr>
                        <!-- Modal -->
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="pagination-div">
            {{ $Products->links() }}
        </div>
    </div>
@endsection
@section('out-content')
    <div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <form action="{{url('admin/products/destroy')}}" method="post">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="exampleModalLabel">تأكيد حذف المنتج :  <span id="del_UserName"></span></h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        </button>
                    </div>
                    <div class="modal-body">
                        @csrf
                        <input type="hidden" name="id" id="del_user_id" >
                        <p>هل انت متأكد انك تريد حذف هذا المنتج؟! </p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">إلغاء</button>
                        <button type="submit" class="btn btn-danger">نعم</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
