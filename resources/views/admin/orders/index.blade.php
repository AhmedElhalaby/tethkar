@extends('admin.layouts.app')
@section('style')
    <style>
        .pagination > .active > a, .pagination > .active > a:focus, .pagination > .active > a:hover, .pagination > .active > span, .pagination > .active > span:focus, .pagination > .active > span:hover {
            background-color: #9c27b0;
            border-color: #9c27b0;
        }

        .pagination > li > a, .pagination > li > span {
            color: #9c27b0;
        }

        .pagination > li > a:focus, .pagination > li > a:hover, .pagination > li > span:focus, .pagination > li > span:hover {
            color: #9c27b0;
            box-shadow: 0 1px 4px 0 rgba(0, 0, 0, 0.14);
        }

        .pagination {
            box-shadow: 0 1px 4px 0 rgba(0, 0, 0, 0.14);
        }

        .pagination-div {
            margin-left: 15px;
            margin-right: 15px;
            float: left;
        }
    </style>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header" data-background-color="{{ config('app.color') }}">
                    <h4 class="title">الطلبات</h4>
                    <p class="category">هنا بيانات كل الطلبات </p>
                </div>
                <div class="card-content table-responsive">
                    <table class="table">
                        <thead class="text-primary">
                        <th>id</th>
                        <th>المشتري</th>
                        <th>حالة الطلب</th>
                        <th>حالة الدفع الاكتروني</th>
                        <th>تاريخ الطلب</th>
                        <th>العمليات</th>
                        </thead>
                        <tbody>
                        @foreach($Orders as $item)
                            <tr>
                                <td>{{$item->id}}</td>
                                <td><a href="{{url('admin/users/update/'.$item->user->id)}}">{{$item->user->name}}</a>
                                </td>
                                <td>
                                    @if($item->status == 0)
                                        <label class="label label-info">جديد</label>
                                    @elseif($item->status == 1)
                                        <label class="label label-success">منتهي</label>
                                    @elseif($item->status == 2)
                                        <label class="label label-danger">ملغي</label>
                                    @else
                                        <label class="label label-default">خطأ</label>
                                    @endif

                                </td>
                                <td>
                                    @if($item->payment_type == 1)
                                        @if($item->status_payment == 0)
                                            <label class="label label-danger">لم يتم الدفع</label>
                                        @else
                                            <label class="label label-success">تم الدفع</label>
                                        @endif
                                    @endif

                                </td>


                                <td>{{$item->created_at}}</td>
                                <td class="text-primary">
                                    <a href="{{url('admin/orders/'.$item->id)}}" data-toggle="tooltip"
                                       data-placement="bottom" title="تفاصيل الطلب" class="fs-20"><i
                                            class="fa fa-list"></i></a>
                                    @if($item->status == 0)
                                        <a href="#end" data-toggle="modal" data-target="#end"
                                           onclick="document.getElementById('linkEnd').href = '{{url('admin/orders/end/'.$item->id)}}'"
                                           class="fs-20"><i data-toggle="tooltip" data-placement="bottom"
                                                            title="انهاء الطلب" class="fa fa-check-square-o"></i></a>
                                        <a href="#delete" class="fs-20" data-toggle="modal" data-target="#delete"
                                           onclick="document.getElementById('del_UserName').innerHTML = '{{$item->name}}';document.getElementById('del_user_id').value = '{{$item->id}}'"><i
                                                class="fa fa-window-close" data-toggle="tooltip" data-placement="bottom"
                                                title="إلغاء الطلب"></i></a>
                                    @endif
                                </td>
                            </tr>


                            <!-- Modal -->
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="pagination-div">
            {{ $Orders->links() }}
        </div>
    </div>
@endsection
@section('out-content')
    <div class="modal fade" id="end" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="exampleModalLabel">تأكيد إنهاء الطلب </h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    </button>
                </div>
                <div class="modal-body">
                    <p>هل انت متأكد انك تريد إنهاء الطلب؟! </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">تراجع</button>
                    <button href="" id="linkEnd" onclick="window.location = this.href" class="btn btn-success">نعم
                    </button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <form action="{{url('admin/orders/cancel')}}" method="post">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="exampleModalLabel">تأكيد إلغاء الطلب : <span
                                id="del_UserName"></span></h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        </button>
                    </div>
                    <div class="modal-body">
                        @csrf
                        <input type="hidden" name="id" id="del_user_id">
                        <p>هل انت متأكد انك تريد إلغاء الطلب؟! </p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">تراجع</button>
                        <button type="submit" class="btn btn-danger">نعم</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
