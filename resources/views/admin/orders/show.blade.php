@extends('admin.layouts.app')
@section('style')
    <style>
        .pagination > .active > a, .pagination > .active > a:focus, .pagination > .active > a:hover, .pagination > .active > span, .pagination > .active > span:focus, .pagination > .active > span:hover {
            background-color: #9c27b0;
            border-color: #9c27b0;
        }

        .pagination > li > a, .pagination > li > span {
            color: #9c27b0;
        }

        .pagination > li > a:focus, .pagination > li > a:hover, .pagination > li > span:focus, .pagination > li > span:hover {
            color: #9c27b0;
            box-shadow: 0 1px 4px 0 rgba(0, 0, 0, 0.14);
        }

        .pagination {
            box-shadow: 0 1px 4px 0 rgba(0, 0, 0, 0.14);
        }

        .pagination-div {
            margin-left: 15px;
            margin-right: 15px;
            float: left;
        }

        .print:hover {
            cursor: pointer;
        }
    </style>
@endsection
@section('head-icon')
    <a class="print" style="display: inline-block;">
        <i class="material-icons text-primary" data-toggle="tooltip" data-placement="bottom" title="طباعة الطلب"
           style="font-size: 30px">print</i>
    </a>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header" data-background-color="{{ config('app.color') }}">
                    <h4 class="title">تفاصيل الطلب الطلب</h4>
                    <p class="category">هنا كل تفاصيل الطلب</p>
                </div>
                <div class="card-content">
                    <div class="row">
                        <div class="col-md-2">
                            <label class="control-label"></label>
                            <img id="blah" src="{{asset('public/assets/img/user.jpg')}}"
                                 style="width: 125px;height: 125px;fill:red" alt="upload image" class="thumbnail"/>
                        </div>
                        <div class="col-md-4 border-left" style="border-left: 1px #00bcd4 solid">
                            <label class="control-label"> بيانات المشتري </label>
                            <p style="font-size: 16px; margin-top: 5px"><i data-toggle="tooltip" data-placement="bottom"
                                                                           title="اسم المشتري"
                                                                           class="fa fa-user text-gray"></i> <a
                                    href="{{url('admin/users/update/'.$Order->user->id)}}">{{$Order->user->name}}</a>
                            </p>
                            <p style="font-size: 16px; margin-top: 5px"><i data-toggle="tooltip" data-placement="bottom"
                                                                           title="ايميل المشتري"
                                                                           class="fa fa-envelope text-gray"></i> <a
                                    href="mailto:{{$Order->user->email}}">{{$Order->user->email}}</a></p>
                            <p style="font-size: 16px; margin-top: 5px"><i data-toggle="tooltip" data-placement="bottom"
                                                                           title="هاتف المشتري"
                                                                           class="fa fa-phone text-gray"></i>
                                <span>{{$Order->user->phone}}</span></p>
                            <p style="font-size: 16px; margin-top: 5px"><i data-toggle="tooltip" data-placement="bottom"
                                                                           title="عنوان المشتري"
                                                                           class="fa fa-globe text-gray"></i>
                                <span>{{$Order->user->address}}</span></p>
                            <p><i data-toggle="tooltip" data-placement="bottom"
                                  title="رقم الطلب"
                                  class="fa fa-globe text-gray"></i>
                                <span>{{$Order->id}}</span></p>
                        </div>
                        <div class="col-md-6">
                            <label class="control-label"> تفاصيل الدفع والتوصيل </label>
                            <p style="font-size: 16px; margin-top: 5px"><i data-toggle="tooltip" data-placement="bottom"
                                                                           title="طريقة التوصيل"
                                                                           class="fa fa-truck text-gray"></i> <a
                                    href="{{url('admin/delivery_options/update/'.$Order->delivery_option->id)}}">{{$Order->delivery_option->ar_name}}
                                    : {{$Order->delivery_option->price}}</a></p>
                            <p style="font-size: 16px; margin-top: 5px"><i data-toggle="tooltip" data-placement="bottom"
                                                                           title="كود الخصم"
                                                                           class="fa fa-tags text-gray"></i>
                                <span>@if($Order->promo_code_id) {{$Order->promo_code->code .' - %'. $Order->promo_code->value}} @else
                                        لا يوجد كود خصم @endif</span></p>
                            <p style="font-size: 16px; margin-top: 5px"><i data-toggle="tooltip" data-placement="bottom"
                                                                           title="طريقة الدفع والمبلغ الإجمالي"
                                                                           class="fa fa-money text-gray"></i> <span>
                                    @if($Order->payment_type ==1)
                                        <span>نقدا عند التسليم</span>
                                    @else
                                        <span>تحويل بنكي</span>
                                        <a href="{{asset($Order->payment_attachment)}}" download
                                           class="btn btn-sm btn-default" style="padding:5px;"><i
                                                class="fa fa-cloud-download"></i></a>
                                    @endif - {{$Order->total_price}}  ريال
                                </span>
                            </p>
                            <p style="font-size: 16px; margin-top: 5px"><i data-toggle="tooltip" data-placement="bottom"
                                                                           title="حالة الطلب"
                                                                           class="fa fa-tasks text-gray"></i>
                                @if($Order->status == 0)
                                    <span class="text-info">جديد</span>
                                @elseif($Order->status == 1)
                                    <span class="text-success">منتهي</span>
                                @elseif($Order->status == 2)
                                    <span class="text-danger">ملغي</span>
                                @else
                                    <span class="text-default">خطأ</span>
                                @endif
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="card">
                <div class="card-header" data-background-color="{{ config('app.color') }}">
                    <h4 class="title">منتجات الطلب</h4>
                    <p class="category">هنا كل منتجات الطلب</p>
                </div>
                <div class="card-content table-responsive">
                    <table class="table">
                        <thead class="text-primary">
                        <th>المنتج</th>
                        <th>الكمية</th>
                        <th>اجمالي السعر</th>
                        <th> تغليف فاخر</th>
                        <th>  بطاقة اهداء</th>
                        <th>النص المقترح</th>
                        <th> المرسل</th>
                        <th> المرسل الليه</th>
                        <th>النص المطلوب كتابته</th>
                        <th>المرفقات</th>
                        </thead>
                        <tbody>
                        @foreach($Order->items as $item)
                            <tr>
                                <td>
                                    <a href="{{url('admin/products/update/'.$item->product->id)}}">{{$item->product->name}}</a>
                                </td>
                                <td>{{$item->quantity}}</td>
                                <td>{{$item->quantity * $item->product->price}}</td>
                                <td>@if(isset($Order->luxuryPackaging->id))<a href="{{url('admin/luxury_packaging/edit',$Order->luxuryPackaging->id)}}"><img
                                            src="{{asset($Order->luxuryPackaging->image)}}" alt=""
                                            style="width: 100px;height: 100px;">  </a>@endif</td>
                                <td>@if(isset($Order->giftCard->id)) <a href="{{url('admin/gift_cards/edit',$Order->giftCard->id)}}"><img
                                            src="{{asset($Order->giftCard->image)}}" alt=""
                                            style="width: 100px;height: 100px;"> </a>@endif</td>

                                @foreach($item->orderItemDetails as $itemDetails )
                                    @if($loop->index = 0)
                                        <td>
                                            @if(isset($itemDetails->text_design->text_category) )
                                                <a href="{{url('admin/text_categories/text/'.$itemDetails->text_design->text_category->id.'/update/'.$itemDetails->text_design->id)}}"><img
                                                        src="{{asset($itemDetails->text_design->image)}}" alt=""
                                                        style="width: 100px;height: 100px;"></a>
                                            @endif
                                        </td>
                                        <td>{{$itemDetails->sender_name}}</td>
                                        <td>{{$itemDetails->receiver_name}}</td>
                                        <td>{{$itemDetails->description}}</td>
                                        <td>@if($itemDetails->attachment)<img src="{{asset($itemDetails->attachment)}}"
                                                                              alt=""
                                                                              style="width: 100px;height: 100px;">@endif
                                        </td>
                            @else
                                <tr>
                                    <td>
                                    </td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td>
                                        @if(isset($itemDetails->text_design->text_category) )
                                            <a href="{{url('admin/text_categories/text/'.$itemDetails->text_design->text_category->id.'/update/'.$itemDetails->text_design->id)}}"><img
                                                    src="{{asset($itemDetails->text_design->image)}}" alt=""
                                                    style="width: 100px;height: 100px;"></a>
                                        @endif</td>
                                    <td>{{$itemDetails->sender_name}}</td>
                                    <td>{{$itemDetails->receiver_name}}</td>
                                    <td>{{$itemDetails->description}}</td>
                                    <td>
                                        @foreach($itemDetails->OrderItemAttachment as $OrderItemAttachment)
                                            @if($OrderItemAttachment->attachment)
                                                <img src="{{asset($OrderItemAttachment->attachment)}}"
                                                     alt=""
                                                     style="width: 100px;height: 100px;">
                                            @endif
                                        @endforeach
                                    </td>
                                </tr>
                                @endif
                                @endforeach

                                </tr>
                                @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="row" id="order" dir="rtl" style="display:none;" >
        <style>
            #order{
                direction: rtl;
            }
            .card {
                display: inline-block;
                position: relative;
                width: 100%;
                margin: 25 px 0;
                box-shadow: 0 1px 4px 0 rgb(0 0 0 / 14%);
                border-radius: 3px;
                color: rgba(0,0,0, 0.87);
                background: #fff;
            }
            .card .card-header .title {
                color: #FFFFFF;
            }
            .card .card-content {
                padding: 15px 20px;
            }
            .card [data-background-color="blue"] {
                background: linear-gradient(
                    60deg, #26c6da, #00acc1);
                box-shadow: 0 12px 20px -10px rgb(0 188 212 / 28%), 0 4px 20px 0px rgb(0 0 0 / 12%), 0 7px 8px -5px rgb(0 188 212 / 20%);
            }
            element.style {
            }
            .card [data-background-color="blue"] {
                background: linear-gradient(
                    60deg, #26c6da, #00acc1);
                box-shadow: 0 12px 20px -10px rgb(0 188 212 / 28%), 0 4px 20px 0px rgb(0 0 0 / 12%), 0 7px 8px -5px rgb(0 188 212 / 20%);
            }
            .card [data-background-color] {
                color: #FFFFFF;
            }
            .card [data-background-color="blue"] {
                background: linear-gradient(
                    60deg, #26c6da, #00acc1);
                box-shadow: 0 12px 20px -10px rgb(0 188 212 / 28%), 0 4px 20px 0px rgb(0 0 0 / 12%), 0 7px 8px -5px rgb(0 188 212 / 20%);
            }
            .card .card-header {
                margin: -20px 15px 0;
                border-radius: 3px;
                padding: 15px;
            }
            .card .card-content {
                padding: 15px 20px;
            }
            .col-md-2 {
                width: 16.66666667%;
            }
            .card img {
                width: 100%;
                height: auto;
            }
            .thumbnail {
                display: block;
                padding: 4px;
                margin-bottom: 20px;
                line-height: 1.42857143;
                background-color: #fff;
                border: 1px solid #ddd;
                border-radius: 4px;
                -webkit-transition: border .2s ease-in-out;
                -o-transition: border .2s ease-in-out;
                transition: border .2s ease-in-out;
            }
            td{
                padding-left: 15px;
                padding-right: 15px;
            }
            html[data-color="blue"] .text-primary {
                color: rgb(0, 188, 212);
            }.table > thead > tr > th {
                 padding-bottom: 4px;
             }
            .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
                padding: 12px 8px;
                vertical-align: middle;
            }
            .table>tbody>tr>td{
                padding: 8px;
                line-height: 1.42857143;
                vertical-align: top;
                border-top: 1px solid #ddd;
            }
        </style>
        <div class="col-md-12" dir="rtl">
            <div class="card">
                <div class="card-header" data-background-color="{{ config('app.color') }}">
                    <h4 class="title">تفاصيل الطلب الطلب</h4>
                    <p class="category">هنا كل تفاصيل الطلب</p>
                </div>
                <div class="card-content">
                    <table cellspacing="0" width="100%" style="padding: 0px">
                        <thead>
                        <tr>
                            <td width="16.66666667%"><img id="blah" src="{{asset('public/assets/img/user.jpg')}}" style="width: 125px;height: 125px;fill:red" alt="upload image" class="thumbnail" /></td>
                            <td width="33.33333333%" style="text-align: right;border-left: 1px #00bcd4 solid">
                                <label class="control-label"> بيانات المشتري </label>
                                <p style="font-size: 16px; margin-top: 5px"><i data-toggle="tooltip" data-placement="bottom" title="اسم المشتري"  class="fa fa-user text-gray"></i> <a href="{{url('admin/users/update/'.$Order->user->id)}}">{{$Order->user->name}}</a></p>
                                <p style="font-size: 16px; margin-top: 5px"><i data-toggle="tooltip" data-placement="bottom" title="ايميل المشتري"  class="fa fa-envelope text-gray"></i> <a href="mailto:{{$Order->user->email}}">{{$Order->user->email}}</a></p>
                                <p style="font-size: 16px; margin-top: 5px"><i data-toggle="tooltip" data-placement="bottom" title="هاتف المشتري"  class="fa fa-phone text-gray"></i> <span>{{$Order->user->phone}}</span></p>
                                <p style="font-size: 16px; margin-top: 5px"><i data-toggle="tooltip" data-placement="bottom" title="عنوان المشتري"  class="fa fa-globe text-gray"></i> <span>{{$Order->user->address}}</span></p>
                                <p><i data-toggle="tooltip" data-placement="bottom"
                                      title="رقم الطلب"
                                      class="fa fa-globe text-gray"></i>
                                    <span>{{$Order->id}}</span></p>
                            </td>
                            <td width="50%%" style="text-align: right;float: right">
                                <label class="control-label"> تفاصيل الدفع والتوصيل </label>
                                <p style="font-size: 16px; margin-top: 5px"><i data-toggle="tooltip" data-placement="bottom" title="طريقة التوصيل"  class="fa fa-truck text-gray"></i> <a href="{{url('admin/delivery_options/update/'.$Order->delivery_option->id)}}">{{$Order->delivery_option->ar_name}} : {{$Order->delivery_option->price}}</a></p>
                                <p style="font-size: 16px; margin-top: 5px"><i data-toggle="tooltip" data-placement="bottom" title="كود الخصم"  class="fa fa-tags text-gray"></i> <span>@if($Order->promo_code_id) {{$Order->promo_code->code .' - %'. $Order->promo_code->value}} @else لا يوجد كود خصم @endif</span></p>
                                <p style="font-size: 16px; margin-top: 5px"><i data-toggle="tooltip" data-placement="bottom" title="طريقة الدفع والمبلغ الإجمالي"  class="fa fa-money text-gray"></i> <span>
                                        @if($Order->payment_type ==1)
                                            <span>نقدا عند التسليم</span>
                                        @else
                                            <span>تحويل بنكي</span>
                                            <a href="{{asset($Order->payment_attachment)}}" download class="btn btn-sm btn-default" style="padding:5px;"><i class="fa fa-cloud-download"></i></a>
                                        @endif - {{$Order->total_price}}  ريال
                                    </span>
                                </p>
                                <p style="font-size: 16px; margin-top: 5px"><i data-toggle="tooltip" data-placement="bottom" title="حالة الطلب"  class="fa fa-tasks text-gray"></i>
                                    @if($Order->status == 0)
                                        <span class="text-info">جديد</span>
                                    @elseif($Order->status == 1)
                                        <span class="text-success">منتهي</span>
                                    @elseif($Order->status == 2)
                                        <span class="text-danger">ملغي</span>
                                    @else
                                        <span class="text-default">خطأ</span>
                                    @endif
                                </p></td>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-md-12"  dir="rtl">
            <div class="card">
                <div class="card-header" data-background-color="{{ config('app.color') }}">
                    <h4 class="title">منتجات الطلب</h4>
                    <p class="category">هنا كل منتجات الطلب</p>
                </div>
                <div class="card-content table-responsive">
                    <table class="table">
                        <thead class="text-primary">
                        <th>المنتج</th>
                        <th>الكمية</th>
                        <th>اجمالي السعر</th>
                        <th> تغليف فاخر</th>
                        <th>  بطاقة اهداء</th>
                        <th>النص المقترح</th>
                        <th> المرسل</th>
                        <th> المرسل الليه</th>
                        <th>النص المطلوب كتابته</th>
                        <th>المرفقات</th>
                        </thead>
                        <tbody>
                        @foreach($Order->items as $item)
                            <tr>
                                <td>
                                    <a href="{{url('admin/products/update/'.$item->product->id)}}">{{$item->product->name}}</a>
                                </td>
                                <td>{{$item->quantity}}</td>
                                <td>{{$item->quantity * $item->product->price}}</td>
                                <td>@if(isset($Order->luxuryPackaging->id))<a href="{{url('admin/luxury_packaging/edit',$Order->luxuryPackaging->id)}}"><img
                                            src="{{asset($Order->luxuryPackaging->image)}}" alt=""
                                            style="width: 100px;height: 100px;">  </a>@endif</td>
                                <td>@if(isset($Order->giftCard->id)) <a href="{{url('admin/gift_cards/edit',$Order->giftCard->id)}}"><img
                                            src="{{asset($Order->giftCard->image)}}" alt=""
                                            style="width: 100px;height: 100px;"> </a>@endif</td>
                                @foreach($item->orderItemDetails as $itemDetails )
                                    @if($loop->index = 0)
                                        <td>
                                            @if(isset($itemDetails->text_design->text_category) )
                                                <a href="{{url('admin/text_categories/text/'.$itemDetails->text_design->text_category->id.'/update/'.$itemDetails->text_design->id)}}"><img
                                                        src="{{asset($itemDetails->text_design->image)}}" alt=""
                                                        style="width: 100px;height: 100px;"></a>
                                            @endif</td>
                                        <td>{{$itemDetails->sender_name}}</td>
                                        <td>{{$itemDetails->receiver_name}}</td>
                                        <td>{{$itemDetails->description}}</td>
                                        <td>@if($itemDetails->attachment)<img src="{{asset($itemDetails->attachment)}}"
                                                                              alt=""
                                                                              style="width: 100px;height: 100px;">@endif
                                        </td>
                            @else
                                <tr>
                                    <td>
                                    </td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td>
                                        @if(isset($itemDetails->text_design->text_category) )
                                            <a href="{{url('admin/text_categories/text/'.$itemDetails->text_design->text_category->id.'/update/'.$itemDetails->text_design->id)}}"><img
                                                    src="{{asset($itemDetails->text_design->image)}}" alt=""
                                                    style="width: 100px;height: 100px;"></a>
                                        @endif</td>
                                    <td>{{$itemDetails->sender_name}}</td>
                                    <td>{{$itemDetails->receiver_name}}</td>
                                    <td>{{$itemDetails->description}}</td>
                                    <td>
                                        @foreach($itemDetails->OrderItemAttachment as $OrderItemAttachment)
                                            @if($OrderItemAttachment->attachment)
                                                <img src="{{asset($OrderItemAttachment->attachment)}}"
                                                     alt=""
                                                     style="width: 100px;height: 100px;">
                                            @endif
                                        @endforeach
                                    </td>
                                </tr>
                                @endif
                                @endforeach

                                </tr>
                                @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('script')
    <script type='text/javascript'>
        $(function () {
            $(".print").on('click', function () {
                console.log('sdd');
                var w = window.open('Print');
                w.document.write($('#order').html());
                w.print();
                w.close();
            });
        });
    </script>
@endsection
