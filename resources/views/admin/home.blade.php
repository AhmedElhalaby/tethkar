@extends('admin.layouts.app')

@section('content')
    <div class="row">
        <div class="col-lg-4 col-md-6 col-sm-6" onclick="window.location='{{url('admin/users')}}'" style="cursor: pointer">
            <div class="card card-stats">
                <div class="card-header" data-background-color="blue">
                    <i class="material-icons">person</i>
                </div>
                <div class="card-content">
                    <p class="category">مستخدمين</p>
                    <h3 class="title">{{\App\Models\User::where('type',1)->count()}}</h3>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-6">
            <div class="card card-stats" onclick="window.location='{{url('admin/orders')}}'" style="cursor: pointer">
                <div class="card-header" data-background-color="orange">
                    <i class="material-icons">shopping_cart</i>
                </div>
                <div class="card-content">
                    <p class="category">الطلبات</p>
                    <h3 class="title">{{\App\Models\Order::count()}}</h3>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-6" onclick="window.location='{{url('admin/products')}}'" style="cursor: pointer">
            <div class="card card-stats">
                <div class="card-header" data-background-color="red">
                    <i class="material-icons">list_alt</i>
                </div>
                <div class="card-content">
                    <p class="category">المنتجات</p>
                    <h3 class="title">{{\App\Models\Product::all()->count()}}</h3>
                </div>
            </div>
        </div>


    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="card">
                <div class="card-header" data-background-color="{{ config('app.color') }}">
                    <h4 class="title">  ارسال اشعار عام </h4>
                    {{--<p class="category">کاربران جدید در 10 خرداد 1396</p>--}}

                </div>
                <div class="card-content">
                    <form action="{{url('admin/notification/send')}}" method="post">
                        @csrf
                        <div class="row">
                            <div class="col-md-3 btn-group required">
                                <label for="title">العنوان :</label>
                                <input type="text" required="" name="title" id="title" class="form-control" placeholder="ادخل عنوان الاشعار">
                            </div>
                            <div class="col-md-7 btn-group required">
                                <label for="msg">النص :</label>
                                <input type="text" required="" name="msg" id="msg" class="form-control" placeholder="ادخل نص الاشعار">
                            </div>

                            <div class="col-md-1 " style="margin-top: 50px  ">
                                <button type="submit" id="send" class="btn btn-primary">ارسال</button>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-10"></div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
@endsection
