@extends('admin.layouts.app')
@section('style')
    <style>
        .pagination > .active > a, .pagination > .active > a:focus, .pagination > .active > a:hover, .pagination > .active > span, .pagination > .active > span:focus, .pagination > .active > span:hover {
            background-color: #9c27b0;
            border-color: #9c27b0;
        }

        .pagination > li > a, .pagination > li > span {
            color: #9c27b0;
        }

        .pagination > li > a:focus, .pagination > li > a:hover, .pagination > li > span:focus, .pagination > li > span:hover {
            color: #9c27b0;
            box-shadow: 0 1px 4px 0 rgba(0, 0, 0, 0.14);
        }

        .pagination {
            box-shadow: 0 1px 4px 0 rgba(0, 0, 0, 0.14);
        }

        .pagination-div {
            margin-left: 15px;
            margin-right: 15px;
            float: left;
        }
    </style>
@endsection
@section('head-icon')
    <a href="{{url('admin/users/create')}}" style="display: inline-block;">
        <i class="material-icons text-primary" data-toggle="tooltip" data-placement="bottom" title="إضافة مستخدم جديد"
           style="font-size: 30px">add_box</i>
        <p class="hidden-lg hidden-md">Dashboard</p>
    </a>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card card-nav-tabs">
                <div class="card-header" data-background-color="{{ config('app.color') }}">
                    <div class="nav-tabs-navigation">
                        <div class="nav-tabs-wrapper">
                            <span class="nav-tabs-title">المستخدمين:</span>
                            <ul class="nav nav-tabs" data-tabs="tabs">
                                <li class="active">
                                    <a href="#completed" data-toggle="tab" onclick="CompetedPagination()">
                                        <i class="material-icons">bug_report</i>
                                        المكتملين تسجيلهم
                                        <div class="ripple-container"></div>
                                    </a>
                                </li>
                                <li class="">
                                    <a href="#not_completed" data-toggle="tab" onclick="NotCompetedPagination()">
                                        <i class="material-icons">code</i>
                                        غير المكتمل تسجيلهم
                                        <div class="ripple-container"></div>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="card-content">
                    <div class="tab-content">
                        <div class="tab-pane active" id="completed">
                            <table class="table">
                                <thead class="text-primary">
                                <th>الاسم</th>
                                <th>الايميل</th>
                                <th>الهاتف</th>
                                <th>العنوان</th>
                                <th></th>
                                </thead>
                                <tbody>
                                @foreach($Users as $item)
                                    <tr>
                                        <td>{{$item->name}}</td>
                                        <td>{{$item->email}}</td>
                                        <td>{{$item->phone}}</td>
                                        <td>{{$item->address}}</td>
                                        <td class="text-primary">
                                            <a href="{{url('admin/users/update/'.$item->id)}}" data-toggle="tooltip"
                                               data-placement="bottom" title="تعديل" class="fs-20"><i
                                                    class="fa fa-edit"></i></a>
                                            <a href="#EditPassword" class="fs-20" data-toggle="modal"
                                               data-target="#EditPassword"
                                               onclick="document.getElementById('UserName').innerHTML = '{{$item->name}}';document.getElementById('user_id').value = '{{$item->id}}'"><i
                                                    class="fa fa-key" data-toggle="tooltip" data-placement="bottom"
                                                    title="تغيير كلمة المرور"></i></a>
                                            <a href="#delete" class="fs-20" data-toggle="modal" data-target="#delete"
                                               onclick="document.getElementById('del_UserName').innerHTML = '{{$item->name}}';document.getElementById('del_user_id').value = '{{$item->id}}'"><i
                                                    class="fa fa-trash" data-toggle="tooltip" data-placement="bottom"
                                                    title="حذف"></i></a>
                                        </td>
                                    </tr>
                                    <!-- Modal -->
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="tab-pane" id="not_completed">
                            <table class="table">
                                <thead class="text-primary">
                                <th>الاسم</th>
                                <th>الايميل</th>
                                <th>الهاتف</th>
                                <th>العنوان</th>
                                <th></th>
                                </thead>
                                <tbody>
                                @foreach($NotCompletedUsers as $item)
                                    <tr>
                                        <td>{{$item->name}}</td>
                                        <td>{{$item->email}}</td>
                                        <td>{{$item->phone}}</td>
                                        <td>{{$item->address}}</td>
                                        <td class="text-primary">
                                            <a href="{{url('admin/users/update/'.$item->id)}}" data-toggle="tooltip"
                                               data-placement="bottom" title="تعديل" class="fs-20"><i
                                                    class="fa fa-edit"></i></a>
                                            <a href="#delete" class="fs-20" data-toggle="modal" data-target="#delete"
                                               onclick="document.getElementById('del_UserName').innerHTML = '{{$item->name}}';document.getElementById('del_user_id').value = '{{$item->id}}'"><i
                                                    class="fa fa-trash" data-toggle="tooltip" data-placement="bottom"
                                                    title="حذف"></i></a>
                                        </td>
                                    </tr>
                                    <!-- Modal -->
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="completedPagination" class="pagination-div">
            {{ $Users->links() }}
        </div>
        <div id="NotCompletedPagination" class="pagination-div" style="display: none">
            {{ $NotCompletedUsers->links() }}
        </div>
    </div>
@endsection
@section('out-content')
    <div class="modal fade" id="EditPassword" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <form action="{{url('admin/users/update-password')}}" method="post">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="exampleModalLabel">تغيير كلمة المرور : <span id="UserName"></span>
                        </h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        </button>
                    </div>
                    <div class="modal-body">
                        @csrf
                        <input type="hidden" name="id" id="user_id">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group label-floating">
                                    <label for="password" class="control-label">كلمة المرور الجديدة *</label>
                                    <input type="password" id="password" name="password" required
                                           class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}">
                                </div>
                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group label-floating">
                                    <label for="password_confirmation" class="control-label">تأكيد كلمة المرور *</label>
                                    <input type="password" id="password_confirmation" name="password_confirmation"
                                           required
                                           class="form-control {{ $errors->has('password_confirmation') ? ' is-invalid' : '' }}">
                                </div>
                                @if ($errors->has('password_confirmation'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">إغلاق</button>
                        <button type="submit" class="btn btn-primary">حفظ</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <form action="{{url('admin/users/destroy')}}" method="post">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="exampleModalLabel">تأكيد حذف المستخدم : <span
                                id="del_UserName"></span></h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        </button>
                    </div>
                    <div class="modal-body">
                        @csrf
                        <input type="hidden" name="id" id="del_user_id">
                        <p>هل انت متأكد انك تريد حذف هذا الحساب؟! </p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">إلغاء</button>
                        <button type="submit" class="btn btn-danger">نعم</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
@section('script')
    <script>
        function NotCompetedPagination() {
            $('#completedPagination').hide();
            $('#NotCompletedPagination').show();
        }
        function CompetedPagination() {
            $('#completedPagination').show();
            $('#NotCompletedPagination').hide();
        }
    </script>
@endsection
