@extends('admin.layouts.app')
@section('style')
    <style>
        .pagination>.active>a, .pagination>.active>a:focus, .pagination>.active>a:hover, .pagination>.active>span, .pagination>.active>span:focus, .pagination>.active>span:hover {
            background-color: #9c27b0;
            border-color: #9c27b0;
        }
        .pagination>li>a, .pagination>li>span {
            color: #9c27b0;
        }
        .pagination>li>a:focus, .pagination>li>a:hover, .pagination>li>span:focus, .pagination>li>span:hover {
            color: #9c27b0;
            box-shadow: 0 1px 4px 0 rgba(0, 0, 0, 0.14);
        }
        .pagination {
            box-shadow: 0 1px 4px 0 rgba(0, 0, 0, 0.14);
        }
        .pagination-div{
            margin-left: 15px;
            margin-right: 15px;
            float: left;
        }
    </style>
@endsection
@section('head-icon')
    <a href="{{url('admin/users/create')}}" style="display: inline-block;">
        <i class="material-icons text-primary" data-toggle="tooltip" data-placement="bottom" title="إضافة مستخدم جديد" style="font-size: 30px">add_box</i>
        <p class="hidden-lg hidden-md">Dashboard</p>
    </a>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header" data-background-color="{{ config('app.color') }}">
                    <h4 class="title">إضافة مستخدم جديد</h4>
                    <p class="category">إضافة مستخدم جديد</p>
                </div>
                <div class="card-content">
                    <form action="{{url('admin/users/create')}}" method="post" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group label-floating">
                                    <label for="name" class="control-label">الاسم *</label>
                                    <input type="text" id="name" name="name" required class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }}" value="{{old('name')}}">
                                </div>
                                @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group label-floating">
                                    <label for="phone" class="control-label">رقم الهاتف *</label>
                                    <input type="tel" id="phone" name="phone" required class="form-control {{ $errors->has('phone') ? ' is-invalid' : '' }}" value="{{old('phone')}}">
                                </div>
                                @if ($errors->has('phone'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-6">
                                <div class="form-group label-floating">
                                    <label for="email" class="control-label">الايميل *</label>
                                    <input type="email" id="email" name="email" required class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }}" value="{{old('email')}}">
                                </div>
                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group label-floating">
                                    <label for="address" class="control-label">العنوان *</label>
                                    <input type="text" id="address" name="address" required class="form-control {{ $errors->has('address') ? ' is-invalid' : '' }}" value="{{old('address')}}">
                                </div>
                                @if ($errors->has('address'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('address') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group label-floating">
                                    <label for="lng" class="control-label">الاحداثي الطولي </label>
                                    <input type="text" id="lng" name="lng" required class="form-control {{ $errors->has('lng') ? ' is-invalid' : '' }}" value="{{old('lng')}}">
                                </div>
                                @if ($errors->has('lng'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('lng') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-6">
                                <div class="form-group label-floating">
                                    <label for="lat" class="control-label">الاحداثي العرضي </label>
                                    <input type="text" id="lat" name="lat" required class="form-control {{ $errors->has('lat') ? ' is-invalid' : '' }}" value="{{old('lat')}}">
                                </div>
                                @if ($errors->has('lat'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('lat') }}</strong>
                                    </span>
                                @endif
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group label-floating">
                                    <label for="password" class="control-label">كلمة المرور  *</label>
                                    <input type="password" id="password" name="password" required class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}" >
                                </div>
                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-6">
                                <div class="form-group label-floating">
                                    <label for="password_confirmation" class="control-label">تأكيد كلمة المرور *</label>
                                    <input type="password" id="password_confirmation" name="password_confirmation" required class="form-control {{ $errors->has('password_confirmation') ? ' is-invalid' : '' }}" >
                                </div>
                                @if ($errors->has('password_confirmation'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="row">
                            <button type="submit" class="btn btn-primary" style="float: left;margin-left:15px;margin-right:15px;">حفظ</button>
                        </div>
                        <div class="clearfix"></div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('script')
    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah')
                        .attr('src', e.target.result);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
@endsection
