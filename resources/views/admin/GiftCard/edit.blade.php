@extends('admin.layouts.app')
@section('style')
    <style>
        .pagination>.active>a, .pagination>.active>a:focus, .pagination>.active>a:hover, .pagination>.active>span, .pagination>.active>span:focus, .pagination>.active>span:hover {
            background-color: #9c27b0;
            border-color: #9c27b0;
        }
        .pagination>li>a, .pagination>li>span {
            color: #9c27b0;
        }
        .pagination>li>a:focus, .pagination>li>a:hover, .pagination>li>span:focus, .pagination>li>span:hover {
            color: #9c27b0;
            box-shadow: 0 1px 4px 0 rgba(0, 0, 0, 0.14);
        }
        .pagination {
            box-shadow: 0 1px 4px 0 rgba(0, 0, 0, 0.14);
        }
        .pagination-div{
            margin-left: 15px;
            margin-right: 15px;
            float: left;
        }
    </style>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header" data-background-color="{{ config('app.color') }}">
                    <h4 class="title">تعديل  بطاقة اهداء جديد</h4>
                    <p class="category">تعديل   بطاقة اهداء  جديد</p>
                </div>
                <div class="card-content">
                    <form action="{{url('admin/gift_cards/update',$GiftCard->id)}}" method="post" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group label-floating">
                                    <label for="price" class="control-label">السعر *</label>
                                    <input type="number" id="price" name="price" required class="form-control {{ $errors->has('price') ? ' is-invalid' : '' }}" value="{{$GiftCard->price}}">
                                </div>
                                @if ($errors->has('price'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('price') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <label class="control-label">الصورة</label>
                                <img id="blah" onclick="document.getElementById('image').click()" src="{{asset($GiftCard->image)}}" style="width: 125px;height: 125px" alt="upload image" class="thumbnail" />
                            </div>
                            <div class="col-md-3">
                                <div class="form-group label-floating" style="display: none">
                                    <label for="image" class="control-label">الصورة</label>
                                    <input type="file" name="image" id="image"  class="form-control" onchange="readURL(this);">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <button type="submit" class="btn btn-primary" style="float: left;margin-left:15px;margin-right:15px;">حفظ</button>
                        </div>
                        <div class="clearfix"></div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('script')
    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah')
                        .attr('src', e.target.result);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
@endsection
