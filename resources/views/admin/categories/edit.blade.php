@extends('admin.layouts.app')
@section('style')
    <style>
        .pagination>.active>a, .pagination>.active>a:focus, .pagination>.active>a:hover, .pagination>.active>span, .pagination>.active>span:focus, .pagination>.active>span:hover {
            background-color: #9c27b0;
            border-color: #9c27b0;
        }
        .pagination>li>a, .pagination>li>span {
            color: #9c27b0;
        }
        .pagination>li>a:focus, .pagination>li>a:hover, .pagination>li>span:focus, .pagination>li>span:hover {
            color: #9c27b0;
            box-shadow: 0 1px 4px 0 rgba(0, 0, 0, 0.14);
        }
        .pagination {
            box-shadow: 0 1px 4px 0 rgba(0, 0, 0, 0.14);
        }
        .pagination-div{
            margin-left: 15px;
            margin-right: 15px;
            float: left;
        }
    </style>
@endsection
@section('head-icon')
    <a href="{{url('admin/categories/create')}}" style="display: inline-block;">
        <i class="material-icons text-primary" data-toggle="tooltip" data-placement="bottom" title="إضافة تصنيف جديد" style="font-size: 30px">add_box</i>
        <p class="hidden-lg hidden-md">Dashboard</p>
    </a>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header" data-background-color="{{ config('app.color') }}">
                    <h4 class="title">تعديل بيانات التصنيف</h4>
                    <p class="category">تعديل بيانات التصنيف</p>
                </div>
                <div class="card-content">
                    <form action="{{url('admin/categories/update/'.$Category->id)}}" method="post" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group label-floating">
                                    <label for="name" class="control-label">الاسم انجليزي *</label>
                                    <input type="text" id="name" name="name" required class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }}" value="{{$Category->name}}">
                                </div>
                                @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-6">
                                <div class="form-group label-floating">
                                    <label for="ar_name" class="control-label">الاسم عربي *</label>
                                    <input type="text" id="ar_name" name="ar_name" required class="form-control {{ $errors->has('ar_name') ? ' is-invalid' : '' }}" value="{{$Category->ar_name}}">
                                </div>
                                @if ($errors->has('ar_name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('ar_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group label-floating">
                                    <label for="description" class="control-label">الوصف انجليزي *</label>
                                    <input type="text" id="description" name="description" required class="form-control {{ $errors->has('description') ? ' is-invalid' : '' }}" value="{{$Category->description}}">
                                </div>
                                @if ($errors->has('description'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                @endif
                            </div>  <div class="col-md-6">
                                <div class="form-group label-floating">
                                    <label for="ar_description" class="control-label">الوصف عربي *</label>
                                    <input type="text" id="ar_description" name="ar_description" required class="form-control {{ $errors->has('ar_description') ? ' is-invalid' : '' }}" value="{{$Category->ar_description}}">
                                </div>
                                @if ($errors->has('ar_description'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('ar_description') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <label class="control-label">الصورة </label>
                                <img id="blah" onclick="document.getElementById('image ').click()" src="{{asset($Category->image)}}" style="width: 125px;height: 125px" alt="upload attachment" class="thumbnail" />
                            </div>
                            <div class="col-md-3">
                                <div class="form-group label-floating" style="display: none">
                                    <label for="image " class="control-label">الصورة - Min width 500px - 2:1 w:h</label>
                                    <input type="file" name="image" accept="image/*" id="image "  class="form-control" onchange="readURL(this);">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <button type="submit" class="btn btn-primary" style="float: left;margin-left:15px;margin-right:15px;">حفظ التعديلات</button>
                        </div>
                        <div class="clearfix"></div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('script')
    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah')
                        .attr('src', e.target.result);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
@endsection
