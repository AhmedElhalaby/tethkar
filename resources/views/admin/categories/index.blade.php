@extends('admin.layouts.app')
@section('style')
    <style>
        .pagination>.active>a, .pagination>.active>a:focus, .pagination>.active>a:hover, .pagination>.active>span, .pagination>.active>span:focus, .pagination>.active>span:hover {
            background-color: #9c27b0;
            border-color: #9c27b0;
        }
        .pagination>li>a, .pagination>li>span {
            color: #9c27b0;
        }
        .pagination>li>a:focus, .pagination>li>a:hover, .pagination>li>span:focus, .pagination>li>span:hover {
            color: #9c27b0;
            box-shadow: 0 1px 4px 0 rgba(0, 0, 0, 0.14);
        }
        .pagination {
            box-shadow: 0 1px 4px 0 rgba(0, 0, 0, 0.14);
        }
        .pagination-div{
            margin-left: 15px;
            margin-right: 15px;
            float: left;
        }
    </style>
@endsection
@section('head-icon')
    <a href="{{url('admin/categories/create')}}" style="display: inline-block;">
        <i class="material-icons text-primary" data-toggle="tooltip" data-placement="bottom" title="إضافة تصنيف جديد" style="font-size: 30px">add_box</i>
        <p class="hidden-lg hidden-md">Dashboard</p>
    </a>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header" data-background-color="{{ config('app.color') }}">
                    <h4 class="title">التصنيفات</h4>
                    <p class="category">هنا بيانات كل التصنيفات </p>
                </div>
                <div class="card-content table-responsive">
                    <table class="table">
                        <thead class="text-primary">
                            <th>الاسم</th>
                            <th>الوصف</th>
                            <th></th>
                        </thead>
                        <tbody>
                        @foreach($Categories as $item)
                        <tr>
                            <td>{{$item->name . ' , ' . $item->ar_name}}</td>
                            <td>{{$item->description . ' , ' . $item->ar_description}}</td>
                            <td class="text-primary">
                                <a href="{{url('admin/categories/update/'.$item->id)}}" data-toggle="tooltip" data-placement="bottom" title="تعديل" class="fs-20"><i class="fa fa-edit"></i></a>
                                <a href="{{url('admin/categories/sub/'.$item->id)}}" data-toggle="tooltip" data-placement="bottom" title="التصنيفات الفرعية" class="fs-20"><i class="fa fa-list"></i></a>
                                <a href="#delete" class="fs-20" data-toggle="modal" data-target="#delete" onclick="document.getElementById('del_UserName').innerHTML = '{{$item->name}}';document.getElementById('del_user_id').value = '{{$item->id}}'"><i class="fa fa-trash" data-toggle="tooltip" data-placement="bottom" title="حذف"></i></a>
                            </td>
                        </tr>
                        <!-- Modal -->
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="pagination-div">
            {{ $Categories->links() }}
        </div>
    </div>
@endsection
@section('out-content')
    <div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <form action="{{url('admin/categories/destroy')}}" method="post">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="exampleModalLabel">تأكيد حذف التصنيف :  <span id="del_UserName"></span></h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        </button>
                    </div>
                    <div class="modal-body">
                        @csrf
                        <input type="hidden" name="id" id="del_user_id" >
                        <p>هل انت متأكد انك تريد حذف هذا التصنيف؟! </p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">إلغاء</button>
                        <button type="submit" class="btn btn-danger">نعم</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
