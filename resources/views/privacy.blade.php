@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <span style="float: right">الأحكام والشروط</span>
                </div>
                <div class="card-body">
                    <div style="float: right; text-align: right">
                        <div style="float: right; text-align: right;"><p dir="rtl" style="margin-bottom: 0.11in; line-height: 108%;"><font face="Arial"><span lang="ar-SA"><font face="Arial">موافقتك
على الشروط والاحكام ، تعني التزامك بما
يلي </font></span></font>:-</p> <p dir="rtl" style="margin-bottom: 0.11in; line-height: 108%;">&nbsp;</p> <p dir="rtl" style="margin-bottom: 0.11in; line-height: 108%;">1- <font face="Arial"><span lang="ar-SA"><font face="Arial">يلزم
اختيار اسم لائق ومناسب خلال عملية
التسجيل</font></span></font>.</p> <p dir="rtl" style="margin-bottom: 0.11in; line-height: 108%;">2-
                                <font face="Arial"><span lang="ar-SA"><font face="Arial">يلتزم
البائع بعدم ادراج أي منتجات ممنوعة بحسب
الأنظمة المعمول بها في المملكة العربية
السعودية </font></span></font>.</p> <p dir="rtl" style="margin-bottom: 0.11in; line-height: 108%;">3-
                                <font face="Arial"><span lang="ar-SA"><font face="Arial">يلتزم
البائع بإدراج المنتجات بحسب التصنيفات
المتاحة ويحق لإدارة التطبيق نقل او حذف
أي من المنتجات او التصنيفات </font></span></font>.</p> <p dir="rtl" style="margin-bottom: 0.11in; line-height: 108%;">4-
                                <font face="Arial"><span lang="ar-SA"><font face="Arial">يلتزم
البائع بأن تكون الصور المضافة للمنتجات
لنفس المنتج</font></span></font>.</p> <p dir="rtl" style="margin-bottom: 0.11in; line-height: 108%;">5-
                                <font face="Arial"><span lang="ar-SA"><font face="Arial">تتعهد
بعدم اضافة أي ردود او تعليقات غير مناسبة
تحتوي على عبارات سب أو شتم أو بخس لأي من
المنتجات</font></span></font>.</p> <p dir="rtl" style="margin-bottom: 0.11in; line-height: 108%;">6-
                                <font face="Arial"><span lang="ar-SA"><font face="Arial">تتعهد
بعدم انتهاك حقوق الآخرين الملكية أو
الفكرية أو براءة الاختراع</font></span></font>.</p> <p dir="rtl" style="margin-bottom: 0.11in; line-height: 108%;">7-
                                <font face="Arial"><span lang="ar-SA"><font face="Arial">إدارة
التطبيق </font></span></font>( <font face="Arial"><span lang="ar-SA"><font face="Arial">تذكار </font></span></font>) <font face="Arial"><span lang="ar-SA"><font face="Arial">غير
مسؤولة عن أي خلافات قد تحدث بين البائع
والزبون لأي سبب كان </font></span></font>.</p> <p dir="rtl" style="margin-bottom: 0.11in; line-height: 108%;">8-
                                <font face="Arial"><span lang="ar-SA"><font face="Arial">إدارة
التطبيق </font></span></font>( <font face="Arial"><span lang="ar-SA"><font face="Arial">تذكار </font></span></font>) <font face="Arial"><span lang="ar-SA"><font face="Arial">لا
تضمن ثبات وصحة الأسعار المعروضة في
التطبيق </font></span></font>.</p> <p dir="rtl" style="margin-bottom: 0.11in; line-height: 108%;">9- <font face="Arial"><span lang="ar-SA"><font face="Arial">اذا
كان اسم المتجر أو المحل أو العضوية يحتوي
على اسم تجاري او علامة تجارية ، يجب ان
تكون المالك للعلامة التجارية او مخول
لك باستخدام الاسم او العلامة التجارية</font></span></font>.</p> <p dir="rtl" style="margin-bottom: 0.11in; line-height: 108%;">10- <font face="Arial"><span lang="ar-SA"><font face="Arial">يحق
لإدارة التطبيق </font></span></font>( <font face="Arial"><span lang="ar-SA"><font face="Arial">تذكار </font></span></font>) <font face="Arial"><span lang="ar-SA"><font face="Arial">حذف
أو تعديل أي منتج او بيانات مُدخلة أو
عضوية من دون ذكر سبب الحذف أو التعديل</font></span></font>.</p> <p dir="rtl" style="margin-bottom: 0.11in; line-height: 108%;">11- <font face="Arial"><span lang="ar-SA"><font face="Arial">يجب
ان تكون العمليات التجارية مباشرة بين
البائع والزبون وبدون أي مسئولية أو تدخل
من تطبيق </font></span></font>( <font face="Arial"><span lang="ar-SA"><font face="Arial">تذكار </font></span></font>) <font face="Arial"><span lang="ar-SA"><font face="Arial">ونحذر
من تحويل الاموال وننصح بالتعامل المباشر
يداً بيد </font></span></font>.</p> <p dir="rtl" style="margin-bottom: 0.11in; line-height: 108%;">12- <font face="Arial"><span lang="ar-SA"><font face="Arial">يحق
لتطبيق </font></span></font>( <font face="Arial"><span lang="ar-SA"><font face="Arial">تذكار </font></span></font>) <font face="Arial"><span lang="ar-SA"><font face="Arial">التحكم
بالتطبيق سواء تحديث أو اضافة أو تغيير
أو حذف أو غير ذلك وبدون ذكر الأسباب</font></span></font>.</p> <p dir="rtl" style="margin-bottom: 0.11in; line-height: 108%;">13- <font face="Arial"><span lang="ar-SA"><font face="Arial">يحق
لتطبيق </font></span></font>( <font face="Arial"><span lang="ar-SA"><font face="Arial">تذكار </font></span></font>) <font face="Arial"><span lang="ar-SA"><font face="Arial">حذف
أو تعديل أو اضافة على بنود </font></span></font>(
                                <font face="Arial"><span lang="ar-SA"><font face="Arial">الشروط
والحكام </font></span></font>) <font face="Arial"><span lang="ar-SA"><font face="Arial">بدون
سابق انذار ، ويلزم على المُسجل متابعة
الشروط والأحكام بشكل دوري</font></span></font>.</p></div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
