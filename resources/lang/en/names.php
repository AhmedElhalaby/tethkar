<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'users' => [
        'name'=>'Name',
        'email'=>'Email',
        'password'=>'Password',
        'old_password'=>'Current Password',
        'password_confirmation'=>'Password Confirmation',
        'phone'=>'Phone',
        'address'=>'Address',
        'device_token'=>'Device Token',
        'device'=>'Device Type',
        'code'=>'Rest Code',
    ],
    'orders' => [
        'delivery_option_id'=>'Delivery Option',
        'payment_type'=>'Payment Type',
        'payment_attachment'=>'Payment Attachment',
        'items'=>'Products',
        'name'=>'Name',
        'email'=>'Email',
        'phone'=>'Phone',
        'address'=>'Address',
    ],

];
