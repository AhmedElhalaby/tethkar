<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'canceled_successful' => 'Order Canceled',
    'cant_find_order' => 'Order not exists',
    'promo_available' => 'Promo code is available',
    'promo_expired' => 'Promo code is expired',
    'promo_not_exist' => 'Promo code is not exists',
    'add_successful' => 'Order placed successfully',

];
