<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'unauthenticated' => 'You have to sign in before',
    'logout' => 'Loged out successfully',
    'login' => 'Loged in successfully',
    'registration_successful' => 'Registerd Successfully',
    'update_successful' => 'Profile Updated Successfully',
    'update_password_successful' => 'Password Updated Successfully',
    'password_not_correct' => 'Password is not correct',
    'code_sent' => 'Reset password code has been sent',
    'code_not_correct' => 'Reset password code is not correct',
    'password_rested' => 'Password Reseted Successfully',
    'refreshed' => 'Refreshed Successfully',

];
