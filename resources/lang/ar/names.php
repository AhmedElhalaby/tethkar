<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'users' => [
        'name'=>'الاسم',
        'email'=>'البريد الالكتروني',
        'password'=>'كلمة المرور',
        'old_password'=>'كلمة المرور الحالية',
        'password_confirmation'=>'تأكيد كلمة المرور',
        'phone'=>'رقم الجوال',
        'address'=>'العنوان',
        'device_token'=>'كود الجهاز',
        'device'=>'نوع الجهاز',
        'code'=>'كود الاسترجاع',
    ],
    'orders' => [
        'delivery_option_id'=>'طريقة التوصيل',
        'payment_type'=>'طريقة الدفع',
        'payment_attachment'=>'مرفق الدفع',
        'items'=>'المنتجات',
        'name'=>'الاسم',
        'email'=>'البريد الالكتروني',
        'phone'=>'رقم الجوال',
        'address'=>'العنوان',
    ],

];
