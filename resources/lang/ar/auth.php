<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'unauthenticated' => 'يجب تسجيل الدخول اولا',
    'failed' => 'البيانات المدخلة خاطئة',
    'logout' => 'تم تسجيل الخروج بنجاح',
    'login' => 'تم تسجيل الدخول بنجاح',
    'registration_successful' => 'تم تسجيل حساب جديد بنجاح',
    'update_successful' => 'تم تعديل البيانات بنجاح',
    'update_password_successful' => 'تم تعديل كلمة المرور بنجاح',
    'password_not_correct' => 'كلمة المرور خاطئة',
    'code_sent' => 'تم ارسال كود استرجاع كلمة المرور',
    'code_not_correct' => 'كود استرجاع كلمة المرور خاطئ',
    'password_rested' => 'تم استرجاع كلمة المرور بنجاح',
    'refreshed' => 'تم التحديث بنجاح',
    'throttle' => 'محاولات تسجيل خاطئة عديدة . يرجى المحاولة بعد  :seconds ثانية.',

];
