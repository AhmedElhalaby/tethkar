<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'canceled_successful' => 'تم الغاء الطلب',
    'cant_find_order' => 'الطلب غير موجود',
    'promo_available' => 'كود الخصم متاح',
    'promo_expired' => 'كود الخصم منتهي',
    'promo_not_exist' => 'كود الخصم غير موجود',
    'add_successful' => 'تم اضافة الطلب بنجاح',

];
