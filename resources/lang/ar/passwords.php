<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'كلمة المرور يجب ان تكون على الاقل 6 احرف وتتطابق التاكيد.',
    'reset' => 'كلمة المرور تم استرجاعها!',
    'sent' => 'تم ارسال كود استرجاع كلمة المرور على الايميل!',
    'token' => 'كود استرجاع كلمة المرور خاطئ.',
    'user' => "لا يوجد مستخدم مرتبط بالايميل المدخل.",

];
