<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderItemDetailsAttachment extends Model
{
    protected $table = 'order_item_details_attachments';
    protected $fillable = ['attachment','order_item_details_id'];

    public function orderItemDetails(){
        return $this->belongsTo(OrderItemDetails::class,'order_item_details_id');
    }


    public function setAttachmentAttribute($value)
    {
        $file = \App\Master::Upload('attachment','order_items/attachment/',$value);
        if($file != false){
            $this->attributes['attachment'] =$file;
        }
    }

}
