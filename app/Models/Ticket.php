<?php

namespace App\Models;

use App\Helpers\Functions;
use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    protected $table = 'tickets';
    protected $fillable = ['user_id','name','email','title','message','attachment','status'];

    public function user(){
        return $this->belongsTo(User::class);
    }
    public function ticket_responses(){
        return $this->hasMany(TicketResponse::class);
    }

    /**
     * @param mixed $attachment
     */
    public function setAttachment($attachment): void
    {
        $this->attachment = Functions::StoreImageModel($attachment,'tickets');
    }
}
