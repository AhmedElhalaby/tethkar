<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderItemDetails extends Model
{
    protected $table = 'order_item_details';
    protected $fillable = ['description', 'attachment', 'sender_name', 'receiver_name', 'text_design_id', 'order_item_id'];

    public function orserItem()
    {
        return $this->belongsTo(OrderItem::class, 'order_item_id');
    }

    public function text_design()
    {
        return $this->belongsTo('App\Models\TextDesign', 'text_design_id', 'id');
    }

    public function OrderItemAttachment()
    {
        return $this->hasMany(OrderItemDetailsAttachment::class, 'order_item_details_id', 'id');
    }

}
