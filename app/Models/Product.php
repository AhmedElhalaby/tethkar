<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';
    protected $fillable = ['name','ar_name','description','ar_description','price','delivery_option','category_id','status'];
    protected $appends = ['Category','images','final_name','final_description'];
    protected static function boot() {
        parent::boot();
        static::addGlobalScope('order',function(Builder $builder){
            $builder->orderBy('created_at', 'desc');
        });
        static::deleting(function($product) {
            File::where('ref_id',$product->id)->delete();
            $orders = OrderItem::where('product_id',$product->id)->pluck('order_id');
            Order::whereIn('id',$orders)->delete();
        });
    }
    public function category(){
        return $this->belongsTo('App\Models\Category','category_id','id');
    }
    public function images_rel(){
        return $this->hasMany('App\Models\File','ref_id','id');
    }
    public function image(){
        return $this->images_rel()->first();
    }
    public function getCategoryAttribute(){
        return $this->category()->first();
    }
    public function getImagesAttribute(){
        return $this->images_rel()->pluck('image');
    }
    public function getFinalNameAttribute(){
        if(app()->getLocale() == 'ar')
            return $this->ar_name;
        return $this->name;
    }
    public function getFinalDescriptionAttribute(){
        if(app()->getLocale() == 'ar')
            return $this->ar_description;
        return $this->description;
    }
}
