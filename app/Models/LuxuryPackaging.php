<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LuxuryPackaging extends Model
{
    protected $table='luxury_packagings';
    protected $fillable=['image','price'];
}
