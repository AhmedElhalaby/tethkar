<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BankAccount extends Model
{
    protected $table = 'bank_accounts';
    protected $fillable = ['bank_name','bank_name_ar','account_num'];
    protected $appends = ['final_bank_name'];

    public function getFinalBankNameAttribute(){
        if(app()->getLocale() == 'ar')
            return $this->bank_name_ar;
        return $this->bank_name;
    }
}
