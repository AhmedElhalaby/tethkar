<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderItem extends Model
{
    protected $table = 'order_items';
    protected $fillable = ['product_id','order_id','quantity','text_design_id','description','attachment'];
    protected $appends = ['total_price','product_name','product_images'];

    public function product(){
        return $this->belongsTo('App\Models\Product','product_id','id');
    }
    public function order(){
        return $this->belongsTo('App\Models\Order','order_id','id');
    }

    public function orderItemDetails(){
        return $this->hasMany(OrderItemDetails::class,'order_item_id','id');
    }
    public function getTotalPriceAttribute(){
        $Product = $this->product()->first();
        return $Product->price * $this->quantity;
    }
    public function getProductNameAttribute(){
        return $this->product()->first()->name;
    }
    public function getProductImagesAttribute(){
        return $this->product()->first()->images;
    }

}
