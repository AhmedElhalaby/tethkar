<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TextCategory extends Model
{
    protected $table = 'text_categories';
    protected $fillable = ['name','ar_name','description','ar_description','status'];
    protected $appends = ['TextDesigns','final_name','final_description'];

    public function text_design(){
        return $this->hasMany('App\Models\TextDesign','text_category_id','id');
    }
    public function getTextDesignsAttribute(){
        return $this->text_design()->get();
    }

    public function getFinalNameAttribute(){
        if(app()->getLocale() == 'ar')
            return $this->ar_name;
        return $this->name;
    }
    public function getFinalDescriptionAttribute(){
        if(app()->getLocale() == 'ar')
            return $this->ar_description;
        return $this->description;
    }
}
