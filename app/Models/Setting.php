<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $table = 'settings';
    protected $fillable = ['key','value','image'];

    public function setImageAttribute($value)
    {
        $file = \App\Master::Upload('image','settings/image/');
        if($file != false){
            $this->attributes['image'] =$file;
        }
    }
}
