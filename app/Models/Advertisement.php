<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Advertisement extends Model
{
    protected $table = 'advertisements';
    protected $fillable = ['name','url','attachment'];

    public function setAttachmentAttribute($value)
    {
        $file = \App\Master::Upload('attachment','categories/image/');
        if($file != false){
            $this->attributes['attachment'] =$file;
        }
    }
}
