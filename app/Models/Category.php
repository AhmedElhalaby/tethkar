<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'categories';
    protected $fillable = ['name','ar_name','description','ar_description','parent','status','image'];
    protected $appends = ['Categories','final_name','final_description'];
    protected static function boot() {
        parent::boot();
        static::deleting(function($category) {
            if($category->parent ==0){
                $ids = Category::where('parent',$category->id)->pluck('id');
                Category::where('parent',$category->id)->delete();
                Product::whereIn('category_id',$ids)->delete();

            }
            Product::where('category_id',$category->id)->delete();
        });
    }
    public function category(){
        $this->belongsTo('App\Models\Category','parent','id');
    }
    public function children() {
        return $this->hasMany(Category::class, 'parent', 'id');
    }

    public function products(){
        $this->hasMany('App\Models\Product','category_id','id');
    }

    public function getCategoriesAttribute(){
        return Category::where('parent',$this->id)->get();
    }
    public function getFinalNameAttribute(){
        if(app()->getLocale() == 'ar')
            return $this->ar_name;
        return $this->name;
    }
    public function getFinalDescriptionAttribute(){
        if(app()->getLocale() == 'ar')
            return $this->ar_description;
        return $this->description;
    }
}
