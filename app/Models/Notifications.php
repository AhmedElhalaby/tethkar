<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Notifications extends Model
{
    protected $table ='notifications';
    protected $fillable =['type','user_id','ref_id','title','msg','read_at'];
    /*---------------------------------------------------------------------*/
    /*----------------------------- Functions -----------------------------*/
    /*---------------------------------------------------------------------*/
    public static function sendNotification($notifiable_id,$token, $title,$msg,$data = null,$type= 1)
    {
        $fcmUrl = 'https://fcm.googleapis.com/fcm/send';
        $registrationIds = $token;


        #prep the bundle
        $message = array
        (
            'body'  => $msg,
            'title' => $title,
            'sound' => true
        );
        $extraNotificationData = ["ref_id" =>$data,"type"=>$type];

        $fields = array
        (
//            'registration_ids'        => $registrationIds,
            'to'        => $registrationIds,
            'notification'  => $message,
            'data' => $extraNotificationData,
            "content_available"=> true,
            "priority"=> "high",
        );

        $headers = array
        (
//            'Authorization: key=AAAAo6tGjZg:APA91bGlIFyTvNFkTadR4u8Qc5U_qxO2J_GoBMjQFgdubxco7qQnw-kt9TidOK4tH5D5XIsFe-XVkl_TUZYmHrNz3SGPT9fJGzF2rLXLPb7Ocn5agfiMkNX0jQsZvDhEtysJLtmjLDyM' ,
            'Authorization: key=AIzaSyB6mAnRpKR93KnIVJx1pvJQ5y7YNWf7U74' ,
            'Content-Type: application/json'
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$fcmUrl);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        curl_close($ch);
        if($notifiable_id != auth()->user()->id){
            $notify = new Notifications();
            $notify->type = $type;
            $notify->user_id = $notifiable_id;
            $notify->title = $title;
            $notify->msg = $msg;
            if($data){
                $notify->ref_id = $data;
            }
            $notify->save();
        }
        return true;
    }
    public static function sendNewNotification($notifiable_id,$token, $title,$msg,$data = null,$type= 1)
    {
        $fcmUrl = 'https://fcm.googleapis.com/fcm/send';
        $registrationIds = $token;


        #prep the bundle
        $message = array
        (
            'body'  => $msg,
            'title' => $title,
            'sound' => true
        );
        $extraNotificationData = ["ref_id" =>$data,"type"=>$type];

        $fields = array
        (
//            'registration_ids'        => $registrationIds,
            'to'        => $registrationIds,
            'notification'  => $message,
            'data' => $extraNotificationData,
            "content_available"=> true,
            "priority"=> "high",
        );

        $headers = array
        (
            'Authorization: key=AAAAo6tGjZg:APA91bGlIFyTvNFkTadR4u8Qc5U_qxO2J_GoBMjQFgdubxco7qQnw-kt9TidOK4tH5D5XIsFe-XVkl_TUZYmHrNz3SGPT9fJGzF2rLXLPb7Ocn5agfiMkNX0jQsZvDhEtysJLtmjLDyM' ,
//            'Authorization: key=AIzaSyB6mAnRpKR93KnIVJx1pvJQ5y7YNWf7U74' ,
            'Content-Type: application/json'
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$fcmUrl);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        curl_close($ch);
        if($notifiable_id != auth()->user()->id){
            $notify = new Notifications();
            $notify->type = $type;
            $notify->user_id = $notifiable_id;
            $notify->title = $title;
            $notify->msg = $msg;
            if($data){
                $notify->ref_id = $data;
            }
            $notify->save();
        }
        return true;
    }
}
