<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * @property mixed prices
 */
class Order extends Model
{
    protected $table = 'orders';
    protected $fillable = ['user_id','promo_code_id','delivery_option_id','payment_type','status_payment','payment_attachment','status','sender_name','receiver_name','luxury_packagings_id','gift_cards_id'];
    protected $appends = ['OrderItems','orderItemDetails','DeliveryOption','User','total_price'];

    protected static function boot() {
        parent::boot();
        static::addGlobalScope('order',function(Builder $builder){
            $builder->orderBy('created_at', 'desc');
        });
        static::deleting(function($order) {
            OrderItem::where('order_id',$order->id)->delete();
        });
    }

    public function user(){
        return $this->belongsTo('App\Models\User','user_id','id');
    }
    public function promo_code(){
        return $this->belongsTo('App\Models\PromoCode','promo_code_id','id');
    }
    public function delivery_option(){
        return $this->belongsTo('App\Models\DeliveryOption','delivery_option_id','id');
    }
    public function items(){
        return $this->hasMany('App\Models\OrderItem','order_id','id');
    }
    public function giftCard(){
        return $this->belongsTo(GiftCard::class,'gift_cards_id');
    }
    public function luxuryPackaging(){
        return $this->belongsTo(LuxuryPackaging::class,'luxury_packagings_id');
    }
    public function getTotalPriceAttribute(){

        $Total = $this->items()->get()->sum('total_price');
        $Promo = $this->promo_code()->first();
        $Total += isset($this->luxuryPackaging->price) ? $this->luxuryPackaging->price  *$this->items()->get()->sum('quantity'): 0;
        $Total += isset($this->giftCard->price) ? $this->giftCard->price  *$this->items()->get()->sum('quantity'): 0;
        $delivery_option = $this->delivery_option()->first();
        if($Promo){
            $discount = ($Total * $Promo->value )/100;
            if($discount > $Promo->limit){
                $discount = $Promo->limit;
            }
            $Total -=$discount;
        }
        $Total +=$delivery_option->price;
        return  round($Total,2);
    }


    public function getUserAttribute(){
        return $this->user()->first();
    }
    public function getDeliveryOptionAttribute(){
        return $this->delivery_option()->first();
    }
    public function getOrderItemsAttribute(){
        return $this->items()->get();
    }
    public function getOrderItemDetailsAttribute(){
        return $this->items[0]->orderItemDetails()->get();
    }

    public function setPaymentAttachmentAttribute($value)
    {
        $file = \App\Master::Upload('payment_attachment','orders/payment_attachment/');
        if($file != false){
            $this->attributes['payment_attachment'] =$file;
        }
    }
}
