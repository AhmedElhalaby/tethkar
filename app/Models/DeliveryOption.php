<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DeliveryOption extends Model
{
    protected $table = 'delivery_options';
    protected $fillable = ['name','ar_name','description','ar_description','price'];
    protected $appends = ['final_name','final_description'];

    public function getFinalNameAttribute(){
        if(app()->getLocale() == 'ar')
            return $this->ar_name;
        return $this->name;
    }
    public function getFinalDescriptionAttribute(){
        if(app()->getLocale() == 'ar')
            return $this->ar_description;
        return $this->description;
    }
}
