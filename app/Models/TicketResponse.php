<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TicketResponse extends Model
{
    protected $table = 'ticket_responses';
    protected $fillable = ['ticket_id','response','sender_type'];

    public function ticket(){
        return $this->belongsTo(Ticket::class);
    }
}
