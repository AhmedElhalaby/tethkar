<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TextDesign extends Model
{
    protected $table = 'text_designs';
    protected $fillable = ['text_category_id','name','image'];


    public function text_category(){
        return $this->belongsTo('App\Models\TextCategory','text_category_id','id');
    }
    public function setImageAttribute($value)
    {
        $file = \App\Master::Upload('image','categories/image/');
        if($file != false){
            $this->attributes['image'] =$file;
        }
    }
}
