<?php


namespace App;


use App\Models\File;

class Master
{
    public static function NiceNames($Model){
        switch ($Model){
            case 'User':
                return [
                    'name'=>__('names.users.name'),
                    'email'=>__('names.users.email'),
                    'password'=>__('names.users.password'),
                    'old_password'=>__('names.users.old_password'),
                    'password_confirmation'=>__('names.users.password_confirmation'),
                    'phone'=>__('names.users.phone'),
                    'address'=>__('names.users.address'),
                    'device_token'=>__('names.users.device_token'),
                    'device'=>__('names.users.device'),
                    'code'=>__('names.users.code'),
                ];
            case 'Order':
                return [
                    'delivery_option_id'=>__('names.orders.delivery_option_id'),
                    'payment_type'=>__('names.orders.payment_type'),
                    'payment_attachment'=>__('names.orders.payment_attachment'),
                    'items'=>__('names.orders.items'),
                    'name'=>__('names.orders.name'),
                    'email'=>__('names.orders.email'),
                    'phone'=>__('names.orders.phone'),
                    'address'=>__('names.orders.address'),
                ];
            default :
                [];
        }

    }

    public static function Upload($attribute_name, $destination_path,$value = null){
        $destination_path = "public/".$destination_path;

        if($value){
            // 1. Generate a new file name
            $file = $value;
            $new_file_name = md5($file->getClientOriginalName().time()).'.'.$file->getClientOriginalExtension();
            // 2. Move the new file to the correct path
            $file_path = $file->move($destination_path, $new_file_name);
            // 3. Save the complete path to the database
            return $destination_path.$new_file_name;
        }
        else{
            $request = \Request::instance();
            // if a new file is uploaded, store it on disk and its filename in the database
            if ($request->hasFile($attribute_name) && $request->file($attribute_name)->isValid()) {
                // 1. Generate a new file name
                $file = $request->file($attribute_name);
                $new_file_name = md5($file->getClientOriginalName().time()).'.'.$file->getClientOriginalExtension();
                // 2. Move the new file to the correct path
                $file_path = $file->move($destination_path, $new_file_name);
                // 3. Save the complete path to the database
                return $destination_path.$new_file_name;
            }
            return false;
        }
    }
    public static function MultiUpload($attribute_name, $destination_path,$type=null,$ref_id= null){
        $request = \Request::instance();
        $destination_path = "public/".$destination_path;

        // if a new file is uploaded, store it on disk and its filename in the database
        if ($request->hasFile($attribute_name)) {
            $file = $request->file($attribute_name);

            if(is_array($file)){
                if($type == null || $ref_id == null)
                    return false;
                foreach ($file as $item){
                    // 1. Generate a new file name
                    $new_file_name = md5($item->getClientOriginalName().time()).'.'.$item->getClientOriginalExtension();
                    // 2. Move the new file to the correct path
                    $file_path = $item->move($destination_path, $new_file_name);
                    // 3. Save the complete path to the database
                    File::create(array('type'=>$type,'ref_id'=>$ref_id,'image'=>$destination_path.$new_file_name));
                }
                return true;
            }else{
                // 1. Generate a new file name
                $file = $request->file($attribute_name);
                $new_file_name = md5($file->getClientOriginalName().time()).'.'.$file->getClientOriginalExtension();
                // 2. Move the new file to the correct path
                $file_path = $file->move($destination_path, $new_file_name);
                // 3. Save the complete path to the database
                return $destination_path.$new_file_name;
            }
        }
        return false;
    }
}
