<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CategoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $Objects = array();
        $Objects['id'] = $this->id;
        $Objects['name'] = $this->name;
        $Objects['ar_name'] =$this->ar_name;
        $Objects['description'] = $this->description;
        $Objects['ar_description'] = $this->ar_description;
        $Objects['image'] = asset($this->image);
        $Objects['parent'] = $this->parent;
        $Objects['status'] = $this->status;
        $Objects['created_at'] = $this->created_at;
        $Objects['updated_at'] = $this->updated_at;
        $Objects['categories'] = CategoryResource::collection($this->getCategoriesAttribute());
        return $Objects;
    }
}
