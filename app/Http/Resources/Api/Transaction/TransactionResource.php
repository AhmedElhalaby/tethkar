<?php

namespace App\Http\Resources\Api\Transaction;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class TransactionResource extends JsonResource
{
    public function toArray($request): array
    {
        $Objects = array();
        $Objects['id'] = $this->id;
        $Objects['type'] = $this->type;
        $Objects['type_str'] = __('crud.Transaction.Types.'.$this->type);
        $Objects['value'] = $this->value;
        $Objects['payment_token'] = $this->payment_token;
        //$Objects['status'] = $this->status;
        $Objects['status_payment_order'] = $this->order->status_payment;
        $Objects['status_str'] = __('crud.Transaction.Statuses.'.$this->status);
        $Objects['created_at'] = ($this->created_at)?Carbon::parse($this->created_at)->format('Y-m-d h:i A'):null;
        return $Objects;
    }
}
