<?php

namespace App\Http\Resources\Api\Ticket;

use Illuminate\Http\Resources\Json\JsonResource;

class TicketResponseResource extends JsonResource
{
    public function toArray($request): array
    {
        $Objects = array();
        $Objects['id'] = $this->id;
        $Objects['response'] = $this->response;
        $Objects['sender_type'] = $this->sender_type;
        return $Objects;
    }
}
