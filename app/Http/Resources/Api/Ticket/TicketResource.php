<?php

namespace App\Http\Resources\Api\Ticket;

use Illuminate\Http\Resources\Json\JsonResource;

class TicketResource extends JsonResource
{
    public function toArray($request): array
    {
        $Objects = array();
        $Objects['id'] = $this->id;
        $Objects['title'] = $this->title;
        $Objects['message'] = $this->message;
        $Objects['attachment'] = asset($this->attachment);
        $Objects['status'] = $this->status;
        $Objects['TicketResponses'] = TicketResponseResource::collection($this->ticket_responses);
        return $Objects;
    }
}
