<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class LuxuryPackagingResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $Objects = array();

        $Objects['id'] = $this->id;
        $Objects['price'] = $this->price;
        $Objects['image'] = asset($this->image);
        return $Objects;
    }
}
