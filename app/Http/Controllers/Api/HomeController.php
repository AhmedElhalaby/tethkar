<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\CategoryResource;
use App\Http\Resources\GiftCardResource;
use App\Http\Resources\LuxuryPackagingResource;
use App\Models\Advertisement;
use App\Models\BankAccount;
use App\Models\Category;
use App\Models\DeliveryOption;
use App\Models\GiftCard;
use App\Models\LuxuryPackaging;
use App\Models\Notifications;
use App\Models\Setting;
use App\Models\TextCategory;
use App\Traits\ResponseTrait;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    use ResponseTrait;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return Renderable
     */
    public function index()
    {
        $Categories = CategoryResource::collection(Category::where('parent', 0)->get());
        $DeliveryOptions = DeliveryOption::all();
        $BankAccounts = BankAccount::all();
        $TextCategories = TextCategory::all();
        $Settings_model = Setting::all();
        $Advertisements = Advertisement::all();
        $Gift_Card = GiftCardResource::collection(GiftCard::all());
        $Luxury_Packaging = LuxuryPackagingResource::collection(LuxuryPackaging::all());
        $Settings = [];
        foreach ($Settings_model as $item) {
            $Settings = array_merge($Settings, [$item->key => $item->value]);
        }
        return $this->successJsonResponse([], [
            'Categories' => $Categories,
            'DeliveryOptions' => $DeliveryOptions,
            'BankAccounts' => $BankAccounts,
            'TextCategories' => $TextCategories,
            'Settings' => $Settings,
            'Advertisements' => $Advertisements,
            'Gift_Card' => $Gift_Card,
            'Luxury_Packaging' => $Luxury_Packaging,
        ]);
    }

    public function notifications(Request $request)
    {
        $per_page = ($request->has('per_page')) ? $request->per_page : 10;
        $Notifications = Notifications::where('user_id', auth()->user()->id)->paginate($per_page);
        return $this->successJsonResponse([], $Notifications->items(), 'Notification', $Notifications);
    }
}
