<?php

namespace App\Http\Controllers\Api;

use App\Models\BankAccount;
use App\Models\Category;
use App\Models\DeliveryOption;
use App\Models\Product;
use App\Models\Setting;
use App\Models\TextCategory;
use App\Traits\ResponseTrait;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    use ResponseTrait;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('auth');
    }

    /**
     *
     * @param Request $request
     * @return Renderable
     */
    public function index(Request $request)
    {
        $Products = new Product();
        if($request->has('category_id')){
            $category = Category::where('id',$request->category_id)->first();
            if($category->parent == 0){
                $categories = Category::where('parent',$category->id)->pluck('id');
                $Products = $Products->whereIn('category_id',$categories);
            }
            else
                $Products = $Products->where('category_id',$request->category_id);
        }
        if($request->has('q')){
            if(app()->getLocale() == 'ar'){
                $Products = $Products->Where('ar_name','LIKE','%'.$request->q.'%')
                    ->orWhere('ar_description','LIKE','%'.$request->q.'%');
            }else{
                $Products = $Products->where('name','LIKE','%'.$request->q.'%')
                    ->orWhere('description','LIKE','%'.$request->q.'%');
            }
        }
        $per_page = ($request->has('per_page'))?$request->per_page:10;
        $Products = $Products->paginate($per_page);
        return $this->successJsonResponse([],$Products->items(),'Products',$Products);
    }

    /**
     *
     * @param $id
     * @return Renderable
     */
    public function show($id)
    {
        return $this->successJsonResponse([],Product::find($id),'Product');
    }
}
