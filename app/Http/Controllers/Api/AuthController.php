<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Auth\FacilityRegistrationForm;
use App\Http\Requests\Api\Auth\ForgetPasswordRequest;
use App\Http\Requests\Api\Auth\LoginForm;
use App\Http\Requests\Api\Auth\PasswordRequest;
use App\Http\Requests\Api\Auth\RefreshForm;
use App\Http\Requests\Api\Auth\RegistrationForm;
use App\Http\Requests\Api\Auth\ResetPasswordRequest;
use App\Http\Requests\Api\Auth\UserRequest;
use App\Traits\ResponseTrait;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    use ResponseTrait;

    /**
     * Create user
     *
     * @param RegistrationForm $form
     * @return ResponseTrait
     */
    public function register(RegistrationForm $form)
    {
        return $form->persist();
    }

    /**
     * Login user and create token
     *
     * @param LoginForm $form
     * @return ResponseTrait
     */
    public function login(LoginForm $form)
    {
        return $form->persist();
    }

    /**
     * Logout user (Revoke the token)
     *
     * @param Request $request
     * @return ResponseTrait
     */
    public function logout(Request $request)
    {
        $request->user()->token()->revoke();
        $request->user()->token()->delete();
        return $this->successJsonResponse([__('auth.logout')]);
    }

    public function show(Request $request)
    {
        return $this->successJsonResponse([],$request->user(),'User');
    }

    /**
     * update user profile
     *
     * @param UserRequest $request
     * @return  ResponseTrait
     */
    public function update(UserRequest $request)
    {
        return $request->update();
    }

    /**
     * Refresh device token
     *
     * @param RefreshForm $request
     * @return  ResponseTrait
     */
    public function refresh(RefreshForm $request){
         return $request->refresh();
    }

    /**
     * @param PasswordRequest $request
     * @return JsonResponse
     */
    public function change_password(PasswordRequest $request){
        return $request->update();
    }

    /**
     * @param ForgetPasswordRequest $request
     * @return JsonResponse
     */
    public function forget_password(ForgetPasswordRequest $request){
        return $request->update();
    }

    /**
     * @param ResetPasswordRequest $request
     * @return JsonResponse
     */
    public function reset_password(ResetPasswordRequest $request){
        return $request->update();
    }
}
