<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Ticket\IndexRequest;
use App\Http\Requests\Api\Ticket\ShowRequest;
use App\Http\Requests\Api\Ticket\StoreRequest;
use App\Http\Requests\Api\Ticket\ResponseRequest;
use App\Http\Resources\CategoryResource;
use App\Models\Category;
use App\Traits\ResponseTrait;
use Illuminate\Http\JsonResponse;

class TicketController extends Controller
{
    use ResponseTrait;
    public function index(IndexRequest $request): JsonResponse
    {

        return $request->run();
    }
    public function show(ShowRequest $request): JsonResponse
    {
        return $request->run();
    }
    public function store(StoreRequest $request): JsonResponse
    {
        return $request->run();
    }
    public function response(ResponseRequest $request): JsonResponse
    {
        return $request->run();
    }



    public function category($id)
    {
        $category = Category::findOrFail( $id);
        $Categories = CategoryResource::collection($category->children);
        return $this->successJsonResponse([], ['Categories' => $Categories,]);
    }

}
