<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Notifications;
use App\Traits\ResponseTrait;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class NotificationController extends Controller
{
    use ResponseTrait;

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $per_page = ($request->has('per_page'))?$request->per_page:10;
        $Notifications = Notifications::where('user_id',auth()->user()->id)->paginate($per_page);
        return $this->successJsonResponse([],$Notifications->items(),'Notification',$Notifications);
    }

    public function update($id)
    {
        $notification=  Notifications::where('id',$id)->where('user_id',auth()->user()->id)->update(array('read_at' => Carbon::now()));
        return $this->successJsonResponse([]);
    }
    public function read_all()
    {
        $notification=  Notifications::where('user_id',auth()->user()->id)->where('read_at',null)->update(array('read_at' => Carbon::now()));
        return $this->successJsonResponse([]);
    }

}
