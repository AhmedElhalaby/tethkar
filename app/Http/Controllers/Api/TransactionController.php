<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\Api\Transaction\CheckPaymentRequest;
use App\Http\Requests\Api\Transaction\GenerateCheckoutRequest;
use App\Http\Requests\Api\Transaction\IndexRequest;
use App\Http\Requests\Api\Transaction\MyBalanceRequest;
use App\Http\Requests\Api\Transaction\RequestRefundRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Transaction;
use App\Helpers\Constant;
use Illuminate\Support\Facades\Auth;
use App\Http\Resources\Api\Transaction\TransactionResource;


class TransactionController extends Controller
{
    public function index(IndexRequest $request)
    {
        return $request->run();
    }
    public function my_balance(MyBalanceRequest $request)
    {

        return $request->run();
    }
    public function generate_checkout(GenerateCheckoutRequest $request)
    {
        $url = "https://test.oppwa.com/v1/checkouts";
        $data = "entityId=8ac7a4c87d36f770017d414680de16e3" .
            "&amount=".$request->value .
            "&currency=SAR" .
            "&paymentType=DB";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Authorization:Bearer OGFjN2E0Yzg3ZDM2Zjc3MDAxN2Q0MTQ1ZmNiYzE2ZGZ8Qm1rSFQ2RWFwag=='));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);// this should be set to true in production
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $responseData = curl_exec($ch);

        if(curl_errno($ch)) {
            return curl_error($ch);
        }
        curl_close($ch);

        $responseData = json_decode($responseData);
        if($responseData->result->code === "000.200.100"){
            $Object = new Transaction();
            $Object->type =Constant::TRANSACTION_TYPES['Deposit'];
            $Object->value=$request->value;
            $Object->status=Constant::TRANSACTION_STATUS['Pending'];
            $Object->payment_token = $responseData->id;
            $Object->user_id =   Auth::id();
            $Object->save();
            return $Object;
        }else{
                return $responseData;
        }
    }
    public function check_payment(CheckPaymentRequest $request)
    {
        return $request->run();
    }

}
