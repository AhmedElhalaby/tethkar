<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\Api\NewOrderForm;
use App\Http\Requests\Api\NewOrderForm_arr;
use App\Http\Requests\Api\NewVOrderForm;
use App\Models\Order;
use App\Models\PromoCode;
use App\Traits\ResponseTrait;
use Carbon\Carbon;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    use ResponseTrait;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('auth');
    }

    /**
     *
     * @param Request $request
     * @return Renderable
     */
    public function index(Request $request)
    {
        $Order = new Order();
        $Order = $Order->where('user_id', auth()->user()->id);
        if ($request->has('status'))
            $Order = $Order->where('status', $request->status);
        $per_page = ($request->has('per_page')) ? $request->per_page : 10;
        $Order = $Order->paginate($per_page);
        return $this->successJsonResponse([], $Order->items(), 'Orders', $Order);
    }

    /**
     *
     * @param $id
     * @return Renderable
     */
    public function show($id)
    {
        return $this->successJsonResponse([], Order::find($id), 'Order');
    }

    /**
     *
     * @param NewOrderForm $request
     * @return Renderable
     */
    public function store(NewOrderForm $request)
    {
        return $request->persist();
    }

    public function store_arr(NewOrderForm_arr $request)
    {
        return $request->persist();
    }

    /**
     *
     * @param NewVOrderForm $request
     * @return Renderable
     */
    public function v_store(NewVOrderForm $request)
    {
        return $request->persist();
    }

    /**
     *
     * @param $id
     * @return Renderable
     */
    public function cancel($id)
    {
        $Order = Order::find($id);
        if ($Order) {
            if ($Order->user_id == auth()->user()->id) {
                $Order->status = 2;
                $Order->save();
                return $this->successJsonResponse([__('order.canceled_successful')], $Order, 'Order');
            }
        }
        return $this->failJsonResponse([__('order.cant_find_order')]);
    }

    public function check(Request $request)
    {
        $promo = PromoCode::where('code', $request->code)->first();
        if ($promo) {
            if ($promo->expire_date > Carbon::now()) {
                return $this->successJsonResponse([__('order.promo_available')], $promo, 'PromoCode');

            } else {
                return $this->failJsonResponse([__('order.promo_expired')]);
            }
        } else {
            return $this->failJsonResponse([__('order.promo_not_exist')]);
        }
    }

}
