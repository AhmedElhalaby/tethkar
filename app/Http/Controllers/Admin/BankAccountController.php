<?php

namespace App\Http\Controllers\Admin;

use App\Models\BankAccount;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class BankAccountController extends Controller
{

    public function index()
    {
        $BankAccounts= BankAccount::paginate(10);
        return view('admin.bank_accounts.index',compact('BankAccounts'));
    }

    public function getUpdate($id)
    {
        $BankAccount= BankAccount::where('id',$id)->first();
        return view('admin.bank_accounts.edit',compact('BankAccount'));
    }
    public function getCreate()
    {
        return view('admin.bank_accounts.create');
    }
    public function postUpdate(Request $request,$id)
    {
        $validator = Validator::make($request->all(), [
            'bank_name' => 'required|string|max:255',
            'bank_name_ar' => 'required|string|max:255',
            'account_num' => 'required|string|max:255',
        ]);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $BankAccount= BankAccount::find($id);
        $BankAccount->update(array('bank_name'=>$request->bank_name,'bank_name_ar'=>$request->bank_name_ar,'account_num'=>$request->account_num));
        return redirect('admin/bank_accounts')->with('status','تم الحفظ بنجاح !');
    }

    public function postCreate(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'bank_name' => 'required|string|max:255',
            'bank_name_ar' => 'required|string|max:255',
            'account_num' => 'required|string|max:255',
        ]);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $user = BankAccount::create(array('bank_name'=>$request->bank_name,'bank_name_ar'=>$request->bank_name_ar,'account_num'=>$request->account_num));
        return redirect('admin/bank_accounts')->with('status','تم الحفظ بنجاح !');
    }


    public function destroy(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $BankAccount= BankAccount::find($request->id);
        $BankAccount->delete();
        return redirect('admin/bank_accounts')->with('status','تم الحذف بنجاح !');

    }

}
