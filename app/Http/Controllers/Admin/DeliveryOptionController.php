<?php

namespace App\Http\Controllers\Admin;

use App\Models\DeliveryOption;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class DeliveryOptionController extends Controller
{

    public function index()
    {
        $DeliveryOptions= DeliveryOption::paginate(10);
        return view('admin.delivery_options.index',compact('DeliveryOptions'));
    }

    public function getUpdate($id)
    {
        $DeliveryOption= DeliveryOption::where('id',$id)->first();
        return view('admin.delivery_options.edit',compact('DeliveryOption'));
    }
    public function getCreate()
    {
        return view('admin.delivery_options.create');
    }
    public function postUpdate(Request $request,$id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'ar_name' => 'required|string|max:255',
            'description' => 'string|max:255',
            'ar_description' => 'string|max:255',
            'price' => 'required|numeric',
        ]);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $DeliveryOption= DeliveryOption::find($id);
        $DeliveryOption->update(array('price'=>$request->price,'name'=>$request->name,'ar_name'=>$request->ar_name,'description'=>$request->description,'ar_description'=>$request->ar_description));
        return redirect('admin/delivery_options')->with('status','تم الحفظ بنجاح !');
    }

    public function postCreate(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'ar_name' => 'required|string|max:255',
            'description' => 'string|max:255',
            'ar_description' => 'string|max:255',
            'price' => 'required|numeric',
        ]);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $user = DeliveryOption::create(array('price'=>$request->price,'name'=>$request->name,'ar_name'=>$request->ar_name,'description'=>($request->description)?$request->description:'','ar_description'=>($request->ar_description)?$request->ar_description:''));
        return redirect('admin/delivery_options')->with('status','تم الحفظ بنجاح !');
    }


    public function destroy(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $DeliveryOption= DeliveryOption::find($request->id);
        $DeliveryOption->delete();
        return redirect('admin/delivery_options')->with('status','تم الحذف بنجاح !');

    }

}
