<?php

namespace App\Http\Controllers\Admin;

use App\Models\Notifications;
use App\Models\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class OrderController extends Controller
{

    public function index(Request $request)
    {
        $Orders= new Order();
        if($request->has('q')){
            $Orders= $Orders->where('name','LIKE','%'.$request->q.'%')-> orderBy('id', 'desc');
        }
        $Orders= $Orders->paginate(10);
        return view('admin.orders.index',compact('Orders'));
    }

    public function show($id)
    {
        $Order= Order::find($id);
        return view('admin.orders.show',compact('Order'));
    }

    public function print($id)
    {
        $Order= Order::find($id);
        return view('admin.orders.print',compact('Order'));
    }

    public function end($id)
    {
        $Order= Order::find($id);
        $Order->status = 1;
        $Order->save();
        $title = 'إشعار';
        $message = 'تم إنهاء طلبك بنجاح !';
        Notifications::sendNotification($Order->user->id,$Order->user->device_token,$title,$message,$Order->id,1);
        Notifications::sendNewNotification($Order->user->id,$Order->user->device_token,$title,$message,$Order->id,1);
        return redirect('admin/orders')->with('status','تم الإنهاء بنجاح !');
    }
    public function cancel(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $Order= Order::find($request->id);
        $Order->status = 2;
        $Order->save();
        $title = 'إشعار';
        $message = 'تم إلغاء طلبك من طرف الإدارة !';
        Notifications::sendNotification($Order->user->id,$Order->user->device_token,$title,$message,$Order->id,2);
        Notifications::sendNewNotification($Order->user->id,$Order->user->device_token,$title,$message,$Order->id,2);
        return redirect('admin/orders')->with('status','تم إلغاء الطلب !');

    }

}
