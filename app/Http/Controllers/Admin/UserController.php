<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{

    public function index()
    {
        $Users = User::where('type', 1)->paginate(10);
        $NotCompletedUsers = User::where('type', 2)->paginate(10);
        return view('admin.users.index', compact('Users', 'NotCompletedUsers'));
    }

    public function getUpdate($id)
    {
        $User = User::where('id', $id)->first();
        return view('admin.users.edit', compact('User'));
    }

    public function getCreate()
    {
        return view('admin.users.create');
    }

    public function postUpdate(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'email' => 'required|string|max:255|unique:users,email,' . $id,
            'address'=>'required|string|max:255',
            'phone'=>'required|numeric',
        ]);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $user = User::find($id);
        $user->update(array('name' => $request->name, 'email' => $request->email,'address' => $request->address,'phone' => $request->phone,
        'lat'=>$request->has('lat')?$request->lat:$user->lat,
        'lng'=>$request->has('lng')?$request->lng:$user->lng,
        ));
        if ($request->hasFile('image')) {
            $user->provider->image = $request->image;
            $user->provider->save();
        }
        return redirect('admin/users')->with('status', 'تم الحفظ بنجاح !');
    }

    public function postCreate(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'password' => 'required|max:255|confirmed|min:6',
            'email' => 'required|string|max:255|unique:users',
            'address'=>'required|string|max:255',
            'phone'=>'required|numeric',
        ]);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $user = User::create(array('name' => $request->name, 'email' => $request->email, 'password' => Hash::make($request->password),'address' => $request->address,'phone' => $request->phone,'type' => 1,
            'lat'=>$request->has('lat')?$request->lat:null,
            'lng'=>$request->has('lng')?$request->lng:null,
        ));
        return redirect('admin/users')->with('status', 'تم الحفظ بنجاح !');
    }

    public function updatePassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'password' => 'required|max:255|confirmed|min:6',
            'id' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $user = User::find($request->id);
        $user->update(array('password' => Hash::make($request->password)));
        return redirect('admin/users')->with('status', 'تم الحفظ بنجاح !');

    }

    public function destroy(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $user = User::find($request->id);
        $user->delete();
        return redirect('admin/users')->with('status', 'تم الحذف بنجاح !');

    }

}
