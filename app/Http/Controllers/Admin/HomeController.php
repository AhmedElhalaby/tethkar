<?php

namespace App\Http\Controllers\Admin;

use App\Http\Resources\CategoryResource;
use App\Models\Category;
use App\Models\Notifications;
use App\Models\User;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return Renderable
     */
    public function index()
    {
        return view('admin.home');
    }

    /**
     * Show the application dashboard.
     *
     * @param Request $request
     * @return Renderable
     */
    public function sendNotification(Request $request){
        $Users = User::where('type',1)->get();
        foreach ($Users as $user) {
            Notifications::sendNotification($user->id,$user->device_token,$request->title,$request->msg,null,0);
            Notifications::sendNewNotification($user->id,$user->device_token,$request->title,$request->msg,null,0);
        }
        return redirect()->back()->with('status','تم الارسال بنجاح !');
    }

}
