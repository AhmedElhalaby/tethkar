<?php

namespace App\Http\Controllers\Admin;

use App\Models\PromoCode;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class PromoCodeController extends Controller
{

    public function index()
    {
        $PromoCodes= PromoCode::paginate(10);
        return view('admin.promo_codes.index',compact('PromoCodes'));
    }

    public function getUpdate($id)
    {
        $PromoCode= PromoCode::where('id',$id)->first();
        return view('admin.promo_codes.edit',compact('PromoCode'));
    }
    public function getCreate()
    {
        return view('admin.promo_codes.create');
    }
    public function postUpdate(Request $request,$id)
    {
        $validator = Validator::make($request->all(), [
            'code' => 'required|string|max:255|unique:promo_codes,code,'. $id,
            'value' => 'required|numeric',
            'limit' => 'required|numeric',
            'expire_date' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $PromoCode= PromoCode::find($id);
        $PromoCode->update(array('code'=>$request->code,'value'=>$request->value,'limit'=>$request->limit,'expire_date'=>$request->expire_date,'use_time'=>$request->use_time));
        return redirect('admin/promo_codes')->with('status','تم الحفظ بنجاح !');
    }

    public function postCreate(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'code' => 'required|string|max:255|unique:promo_codes,code',
            'value' => 'required|numeric',
            'limit' => 'required|numeric',
            'expire_date' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $user = PromoCode::create(array('code'=>$request->code,'value'=>$request->value,'limit'=>$request->limit,'expire_date'=>$request->expire_date,'use_time'=>$request->use_time));
        return redirect('admin/promo_codes')->with('status','تم الحفظ بنجاح !');
    }


    public function destroy(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $PromoCode= PromoCode::find($request->id);
        $PromoCode->delete();
        return redirect('admin/promo_codes')->with('status','تم الحذف بنجاح !');

    }

}
