<?php

namespace App\Http\Controllers\Admin;

use App\Models\TextCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class TextCategoryController extends Controller
{

    public function index()
    {
        $TextCategories= TextCategory::paginate(10);
        return view('admin.text_categories.index',compact('TextCategories'));
    }

    public function getUpdate($id)
    {
        $TextCategory= TextCategory::where('id',$id)->first();
        return view('admin.text_categories.edit',compact('TextCategory'));
    }
    public function getCreate()
    {
        return view('admin.text_categories.create');
    }
    public function postUpdate(Request $request,$id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'ar_name' => 'required|string|max:255',
            'description' => 'string|max:255',
            'ar_description' => 'string|max:255',
        ]);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $TextCategory= TextCategory::find($id);
        $TextCategory->update(array('name'=>$request->name,'ar_name'=>$request->ar_name,'description'=>$request->description,'ar_description'=>$request->ar_description));
        return redirect('admin/text_categories')->with('status','تم الحفظ بنجاح !');
    }

    public function postCreate(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'ar_name' => 'required|string|max:255',
            'description' => 'string|max:255',
            'ar_description' => 'string|max:255',
        ]);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $user = TextCategory::create(array('name'=>$request->name,'ar_name'=>$request->ar_name,'description'=>($request->description)?$request->description:'','ar_description'=>($request->ar_description)?$request->ar_description:''));
        return redirect('admin/text_categories')->with('status','تم الحفظ بنجاح !');
    }


    public function destroy(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $TextCategory= TextCategory::find($request->id);
        $TextCategory->delete();
        return redirect('admin/text_categories')->with('status','تم الحذف بنجاح !');

    }

}
