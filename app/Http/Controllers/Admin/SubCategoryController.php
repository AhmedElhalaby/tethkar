<?php

namespace App\Http\Controllers\Admin;

use App\Master;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class SubCategoryController extends Controller
{

    public function index($id)
    {
        $Parent= Category::where('id',$id)->first();
        $Categories= Category::where('parent',$id)->paginate(10);
        return view('admin.categories.sub_categories.index',compact('Categories','Parent'));
    }

    public function getUpdate($id,$id2)
    {
        $Parent= Category::where('id',$id)->first();
        $Category= Category::where('id',$id2)->first();
        return view('admin.categories.sub_categories.edit',compact('Category','Parent'));
    }
    public function getCreate($id)
    {
        $Parent= Category::where('id',$id)->first();
        return view('admin.categories.sub_categories.create',compact('Parent'));
    }
    public function postUpdate(Request $request,$id,$id2)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'ar_name' => 'required|string|max:255',
            'description' => 'string|max:255',
            'ar_description' => 'string|max:255',
        ]);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $Category= Category::find($id2);
        $image='';
        if (isset($request->image)){
            $image = Master::Upload('image','category/images/',$request->image);
        }
        $Category->update(array(
            'name'=>$request->name,
            'ar_name'=>$request->ar_name,
            'image'=>($request->image  ? $image : $Category->image),
            'description'=>$request->description,
            'ar_description'=>$request->ar_description
        ));

        return redirect('admin/categories/sub/'.$id)->with('status','تم الحفظ بنجاح !');
    }

    public function postCreate(Request $request,$id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'ar_name' => 'required|string|max:255',
            'description' => 'string|max:255',
            'ar_description' => 'string|max:255',
            'image' => 'required',

        ]);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $image = Master::Upload('image','category/images/',$request->image);
        $user = Category::create(array(
            'name'=>$request->name,
            'ar_name'=>$request->ar_name,
            'image'=>$image,
            'description'=>($request->description)?$request->description:'',
            'ar_description'=>($request->ar_description)?$request->ar_description:'',
            'parent'=>$id
        ));
        return redirect('admin/categories/sub/'.$id)->with('status','تم الحفظ بنجاح !');
    }


    public function destroy(Request $request,$id)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $Category= Category::find($request->id);
        $Category->delete();
        return redirect('admin/categories/sub/'.$id)->with('status','تم الحذف بنجاح !');

    }

}
