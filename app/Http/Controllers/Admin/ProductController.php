<?php

namespace App\Http\Controllers\Admin;

use App\Master;
use App\Models\File;
use App\Models\Product;
use Faker\Provider\Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class ProductController extends Controller
{

    public function index()
    {
        $Products= Product::paginate(10);
        return view('admin.products.index',compact('Products'));
    }

    public function getUpdate($id)
    {
        $Product= Product::where('id',$id)->first();
        return view('admin.products.edit',compact('Product'));
    }
    public function getCreate()
    {
        return view('admin.products.create');
    }
    public function postUpdate(Request $request,$id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'ar_name' => 'required|string|max:255',
            'description' => 'string',
            'ar_description' => 'string|',
            'price' => 'numeric',
            'category_id' => 'required|exists:categories,id',
        ]);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $Product= Product::find($id);
        $Product->update(array(
            'name'=>$request->name,
            'ar_name'=>$request->ar_name,
            'description'=>$request->description,
            'ar_description'=>$request->ar_description,
            'price'=>$request->price,
            'delivery_option'=>$request->delivery_option,
            'category_id'=>$request->category_id,
        ));
        if($request->hasFile('image')){
            Master::MultiUpload('image','products/images/',1,$Product->id);
        }
        return redirect('admin/products')->with('status','تم الحفظ بنجاح !');
    }

    public function postCreate(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'ar_name' => 'required|string|max:255',
            'description' => 'string',
            'ar_description' => 'string',
            'price' => 'required|numeric',
            'category_id' => 'required|exists:categories,id',
        ]);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $Product = Product::create(array(
            'name'=>$request->name,
            'ar_name'=>$request->ar_name,
            'category_id'=>$request->category_id,
            'price'=>$request->price,
            'delivery_option'=>$request->delivery_option,
            'description'=>($request->description)?$request->description:'',
            'ar_description'=>($request->ar_description)?$request->ar_description:''
        ));
        Master::MultiUpload('image','products/images/',1,$Product->id);


        return redirect('admin/products')->with('status','تم الحفظ بنجاح !');
    }


    public function destroy(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $Product= Product::find($request->id);
        $Product->delete();
        return redirect('admin/products')->with('status','تم الحذف بنجاح !');

    }

    public function delete_img($id)
    {
        $File= File::find($id);
        $File->delete();
        return response()->json(['status'=>'success']);
    }
    public function end($id)
    {
        $Product= Product::find($id);
        if($Product->status == 1){
            $Product->status = 2;
            $Product->save();
            return redirect('admin/products')->with('status','تم اضافة ملصق نفاذ الكمية !');
        }else{
            $Product->status = 1;
            $Product->save();
            return redirect('admin/products')->with('status','تم إزالة ملصق نفاذ الكمية !');
        }
    }
}
