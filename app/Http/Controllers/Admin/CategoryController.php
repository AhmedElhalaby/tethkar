<?php

namespace App\Http\Controllers\Admin;

use App\Master;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class CategoryController extends Controller
{

    public function index()
    {
        $Categories= Category::where('parent',0)->paginate(10);
        return view('admin.categories.index',compact('Categories'));
    }

    public function getUpdate($id)
    {
        $Category= Category::where('id',$id)->first();
        return view('admin.categories.edit',compact('Category'));
    }
    public function getCreate()
    {
        return view('admin.categories.create');
    }
    public function postUpdate(Request $request,$id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'ar_name' => 'required|string|max:255',
            'description' => 'string|max:255',
            'ar_description' => 'string|max:255',
        ]);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $Category= Category::find($id);
        $image='';
        if (isset($request->image)){
            $image = Master::Upload('image','category/images/',$request->image);
        }
        $Category->update(array('name'=>$request->name,'image'=>($request->image  ? $image : $Category->image),'ar_name'=>$request->ar_name,'description'=>$request->description,'ar_description'=>$request->ar_description));
        return redirect('admin/categories')->with('status','تم الحفظ بنجاح !');
    }

    public function postCreate(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'image' => 'required',
            'ar_name' => 'required|string|max:255',
            'description' => 'string|max:255',
            'ar_description' => 'string|max:255',
        ]);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $image = Master::Upload('image','category/images/',$request->image);
        $user = Category::create(array('name'=>$request->name,'image'=>$image,'ar_name'=>$request->ar_name,'description'=>($request->description)?$request->description:'','ar_description'=>($request->ar_description)?$request->ar_description:''));
        return redirect('admin/categories')->with('status','تم الحفظ بنجاح !');
    }


    public function destroy(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $Category= Category::find($request->id);
        $Category->delete();
        return redirect('admin/categories')->with('status','تم الحذف بنجاح !');

    }

}
