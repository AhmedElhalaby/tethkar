<?php

namespace App\Http\Controllers\Admin;

use App\Models\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class SettingController extends Controller
{

    public function index()
    {
        $Settings= Setting::paginate(10);
        return view('admin.settings.index',compact('Settings'));
    }


    public function getUpdate($id)
    {
        $Setting= Setting::where('id',$id)->first();
        return view('admin.settings.edit',compact('Setting'));
    }

    public function postUpdate(Request $request,$id)
    {
        $validator = Validator::make($request->all(), [
            'value' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $Setting= Setting::find($id);
        $Setting->value = $request->value;
        if($request->hasFile('image')){
            $Setting->image = $request->image;
        }
        $Setting->save();
        return redirect('admin/settings')->with('status','تم الحفظ بنجاح !');
    }
}
