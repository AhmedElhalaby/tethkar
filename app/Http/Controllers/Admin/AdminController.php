<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class AdminController extends Controller
{

    public function index()
    {
        $Users= User::where('type',3)->paginate(10);
        return view('admin.admins.index',compact('Users'));
    }

    public function getUpdate($id)
    {
        $User= User::where('id',$id)->first();
        return view('admin.admins.edit',compact('User'));
    }
    public function getCreate()
    {
        return view('admin.admins.create');
    }
    public function postUpdate(Request $request,$id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'email' => 'required|max:255|unique:users,email,'.$id,
        ]);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $user= User::find($id);
        $user->update(array('name'=>$request->name,'email'=>$request->email));
        if($request->hasFile('image')){
            $user->provider->image = $request->image;
            $user->provider->save();
        }
        return redirect('admin/admins')->with('status','تم الحفظ بنجاح !');
    }

    public function postCreate(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'password' => 'required|confirmed|min:6|max:255',
            'email' => 'required|max:255|unique:users',
        ]);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $user = User::create(array('name'=>$request->name,'email'=>$request->email,'password'=>Hash::make($request->password),'type'=>3));
        return redirect('admin/admins')->with('status','تم الحفظ بنجاح !');
    }

    public function updatePassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'password' => 'required|confirmed|min:6|max:255',
            'id' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $user= User::find($request->id);
        $user->update(array('password'=>Hash::make($request->password)));
        return redirect('admin/admins')->with('status','تم الحفظ بنجاح !');

    }

    public function destroy(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $user= User::find($request->id);
        $user->delete();
        return redirect('admin/admins')->with('status','تم الحذف بنجاح !');

    }

}
