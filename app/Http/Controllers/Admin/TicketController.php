<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\UserManagement\Ticket\CloseRequest;
use App\Http\Requests\Admin\UserManagement\Ticket\ResponseRequest;
use App\Http\Resources\CategoryResource;
use App\Models\Category;
use App\Models\Ticket;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Routing\Redirector;

class TicketController extends Controller
{
    public function index()
    {
        $Products= Ticket::paginate(10);
        return view('admin.ticket.index',compact('Products'));
    }

    /**
     * @param $id
     */
    public function show($id)
    {
        $Object =Ticket::findOrFail($id);
        return view('admin.ticket.show',compact('Object'));
    }

    public function response(ResponseRequest $request, $id){
        return $request->preset($id);
    }
    /**
     * @param CloseRequest $request
     * @param $id
     * @return Application|RedirectResponse|Redirector
     */
    public function close(CloseRequest $request, $id){
        return $request->preset($id);
    }


}
