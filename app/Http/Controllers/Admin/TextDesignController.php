<?php

namespace App\Http\Controllers\Admin;

use App\Models\TextCategory;
use App\Models\TextDesign;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class TextDesignController extends Controller
{

    public function index($id)
    {
        $TextCategory= TextCategory::where('id',$id)->first();
        $TextDesigns= TextDesign::where('text_category_id',$id)->paginate(10);
        return view('admin.text_categories.text_designs.index',compact('TextDesigns','TextCategory'));
    }

    public function getUpdate($id,$id2)
    {
        $TextCategory= TextCategory::where('id',$id)->first();
        $TextDesign= TextDesign::where('id',$id2)->first();
        return view('admin.text_categories.text_designs.edit',compact('TextDesign','TextCategory'));
    }
    public function getCreate($id)
    {
        $TextCategory= TextCategory::where('id',$id)->first();
        return view('admin.text_categories.text_designs.create',compact('TextCategory'));
    }
    public function postUpdate(Request $request,$id,$id2)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'image' => 'image',
        ]);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $TextDesign= TextDesign::find($id2);
        $TextDesign->name = $request->name;
        if($request->image){
            $TextDesign->image = $request->image;
        }
        $TextDesign->save();
        return redirect('admin/text_categories/text/'.$id)->with('status','تم الحفظ بنجاح !');
    }

    public function postCreate(Request $request,$id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'image' => 'required|image',
        ]);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $user = TextDesign::create(array('name'=>$request->name,'image'=>$request->image,'text_category_id'=>$id));
        return redirect('admin/text_categories/text/'.$id)->with('status','تم الحفظ بنجاح !');
    }


    public function destroy(Request $request,$id)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $TextDesign= TextDesign::find($request->id);
        $TextDesign->delete();
        return redirect('admin/text_categories/text/'.$id)->with('status','تم الحذف بنجاح !');

    }

}
