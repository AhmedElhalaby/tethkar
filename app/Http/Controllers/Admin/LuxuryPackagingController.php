<?php

namespace App\Http\Controllers\Admin;

use App\Master;
use App\Models\LuxuryPackaging;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class LuxuryPackagingController extends Controller
{
    public function index()
    {
        $LuxuryPackaging = LuxuryPackaging::paginate(10);
        return view('admin.LuxuryPackaging.index', compact('LuxuryPackaging'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.LuxuryPackaging.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'price' => 'required|numeric',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $image = Master::Upload('image', 'LuxuryPackaging/images/', $request->image);

        $LuxuryPackaging = LuxuryPackaging::create(array(
            'price' => $request->price,
            'image' => $image
        ));


        return redirect('admin/luxury_packaging')->with('status', 'تم الحفظ بنجاح !');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $LuxuryPackaging = LuxuryPackaging::where('id', $id)->first();
        return view('admin.LuxuryPackaging.edit', compact('LuxuryPackaging'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'price' => 'required|numeric',
        ]);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $Category = LuxuryPackaging::find($id);
        $image = '';
        if (isset($request->image)) {
            $image = Master::Upload('image', 'LuxuryPackaging/images/', $request->image);
        }
        $Category->update(array(
            'image' => ($request->image ? $image : $Category->image),
            'price' => $request->price,

        ));
        return redirect('admin/luxury_packaging')->with('status', 'تم الحفظ بنجاح !');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */

    public function destroy(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $LuxuryPackaging = LuxuryPackaging::find($request->id);
        $LuxuryPackaging->delete();
        return redirect('admin/luxury_packaging')->with('status', 'تم الحذف بنجاح !');

    }
}
