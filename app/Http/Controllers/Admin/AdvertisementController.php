<?php

namespace App\Http\Controllers\Admin;

use App\Models\Advertisement;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class AdvertisementController extends Controller
{

    public function index()
    {
        $Advertisements= Advertisement::paginate(10);
        return view('admin.advertisements.index',compact('Advertisements'));
    }

    public function getUpdate($id)
    {
        $Advertisement= Advertisement::where('id',$id)->first();
        return view('admin.advertisements.edit',compact('Advertisement'));
    }
    public function getCreate()
    {
        return view('admin.advertisements.create');
    }
    public function postUpdate(Request $request,$id)
    {
        $validator = Validator::make($request->all(), [
            'attachment' => 'required',
            'name' => 'string|max:255',
        ]);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $Advertisement= Advertisement::find($id);
        $Advertisement->update(array('name'=>$request->name?$request->name:'','url'=>$request->has('url')?$request->url:'','attachment'=>$request->attachment));
        return redirect('admin/advertisements')->with('status','تم الحفظ بنجاح !');
    }

    public function postCreate(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'attachment' => 'required',
            'name' => 'string|max:255',
        ]);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $user = Advertisement::create(array('name'=>$request->name?$request->name:'','url'=>$request->has('url')?$request->url:'','attachment'=>$request->attachment));
        return redirect('admin/advertisements')->with('status','تم الحفظ بنجاح !');
    }


    public function destroy(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $Advertisement= Advertisement::find($request->id);
        $Advertisement->delete();
        return redirect('admin/advertisements')->with('status','تم الحذف بنجاح !');

    }

}
