<?php

namespace App\Http\Requests\Admin\UserManagement\Ticket;

use App\Helpers\Constant;
use App\Helpers\Functions;
use App\Models\Ticket;
use App\Models\TicketResponse;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Http\FormRequest;

class ResponseRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'ticket_response'=>'required|string'
        ];
    }
    public function preset($id){
        $Object = Ticket::findOrFail($id);
        if($Object->status = Constant::TICKETS_STATUS['Open']){
            $Response = new TicketResponse();
            $Response->ticket_id=$Object->id;
            $Response->response=$this->ticket_response;
            $Response->sender_type=Constant::SENDER_TYPE['Admin'];
            $Response->save();
            return redirect()->back()->with('status', 'admin messages saved successfully');
        }
        return '';
    }
}
