<?php

namespace App\Http\Requests\Admin\UserManagement\Ticket;

use App\Helpers\Constant;
use App\Helpers\Functions;
use App\Models\Ticket;
use Illuminate\Foundation\Http\FormRequest;

class CloseRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
        ];
    }
    public function preset($id){
        $Object = Ticket::findOrFail($id);
        $Object->status=Constant::TICKETS_STATUS['Closed'];
        $Object->save();
        return redirect()->back()->with('status', 'admin messages closed successfully');
    }
}
