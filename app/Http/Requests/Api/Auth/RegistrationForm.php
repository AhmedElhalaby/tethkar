<?php

namespace App\Http\Requests\Api\Auth;

use App\Master;
use App\Http\Requests\Api\ApiRequest;
use App\Models\User;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;

class RegistrationForm extends ApiRequest
{
    use ResponseTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6',
            'phone' => 'required|string|min:6',
            'address' => 'nullable|string|min:6',
            'device_token' => 'string|required_with:device',
            'device' => 'string|required_with:device_token',
        ];
    }
    public function attributes()
    {
        return Master::NiceNames('User');
    }
    public function persist()
    {
        $user = new User();
        $user->name = $this->name;
        $user->email = $this->email;
        $user->password = Hash::make($this->password);
        $user->phone = $this->phone;
        $user->address = $this->address;
        $user->lat = $this->has('lat')?$this->lat:null;
        $user->lng = $this->has('lng')?$this->lng:null;
        $user->type = 1;
        if ($this->input('device_token')) {
            $user->device_token = $this->device_token;
            $user->device = $this->device;
        }
        $user->save();

        Auth::attempt(request(['email', 'password']));
        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;
        $token->save();
        $user->refresh();
        $user['access_token']= $tokenResult->accessToken;
        $user['token_type']= 'Bearer';
        $user['expires_at']= Carbon::parse(
            $tokenResult->token->expires_at
        )->toDateTimeString();
        return $this->successJsonResponse( [__('auth.registration_successful')],$user,'User');

    }

}
