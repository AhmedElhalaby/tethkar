<?php

namespace App\Http\Requests\Api\Transaction;

use App\Helpers\Constant;
use App\Helpers\Functions;
use App\Http\Requests\Api\ApiRequest;
use App\Http\Resources\Api\Transaction\TransactionResource;
use App\Models\Transaction;
use App\Traits\ResponseTrait;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;

/**
 * @property mixed value
 * @property mixed payment_token
 */
class GenerateCheckoutRequest extends ApiRequest
{
    public function rules(): array
    {
        return [
            'value'=>'required|numeric',
        ];
    }
    public function run(): JsonResponse
    {
        $id = Functions::GenerateCheckout($this->value);
        if($id['status']){
            $Object = new Transaction();
            $Object->type = Constant::TRANSACTION_TYPES['Deposit'];
            $Object->value  = $this->value ;
            $Object->status  = Constant::TRANSACTION_STATUS['Pending'];
            $Object->payment_token =  $id['id'];
            $Object->user_id  = Auth::user();
            $Object->save();
            return $this->successJsonResponse([],new TransactionResource($Object),'Transaction');
        }else{
            return $this->failJsonResponse([$id['message']]);
        }
    }
}
