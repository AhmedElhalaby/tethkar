<?php

namespace App\Http\Requests\Api\Transaction;

use App\Helpers\Constant;
use App\Helpers\Functions;
use App\Http\Requests\Api\ApiRequest;
use App\Http\Resources\Api\Transaction\TransactionResource;
use App\Models\Notifications;
use App\Models\Order;
use App\Models\Transaction;
use Illuminate\Http\JsonResponse;

/**
 * @property mixed transaction_id
 * @property mixed type
 */
class CheckPaymentRequest extends ApiRequest
{
    public function rules(): array
    {
        return [
            'payment_token'=>'required|exists:transactions,payment_token',
            'ref_id'=>'required|exists:orders,id'
        ];
    }
    public function run(): JsonResponse
    {

        $Object = (new Transaction)->where('payment_token' ,$this->payment_token)->first();


        $Response = Functions::CheckPayment($Object->payment_token);

        if(!$Response){
            return $this->failJsonResponse([__('messages.not_paid_yet')]);
        }
        $Object->status = Constant::TRANSACTION_STATUS['Paid'];
        $Object->ref_id = $this->ref_id;

        $Object->save();
        $Order = Order::findOrFail($this->ref_id);
        $Order->status_payment = 1;
        $Order->save();
        $title = 'إشعار';
        $message = 'تم إنهاء طلبك بنجاح !';
        Notifications::sendNotification($Order->user->id,$Order->user->device_token,$title,$message,$Order->id,1);
        Notifications::sendNewNotification($Order->user->id,$Order->user->device_token,$title,$message,$Order->id,1);
        return redirect('admin/orders')->with('status','تم الإنهاء بنجاح !');

        return $this->successJsonResponse([],new TransactionResource($Object),'Transaction');
    }
}
