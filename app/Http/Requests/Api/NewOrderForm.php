<?php

namespace App\Http\Requests\Api;

use App\Master;
use App\Models\File;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\OrderItemDetails;
use App\Models\OrderItemDetailsAttachment;
use App\Models\Product;
use App\Models\PromoCode;
use App\Models\User;
use App\Traits\ResponseTrait;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class NewOrderForm extends ApiRequest
{
    use ResponseTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'delivery_option_id' => 'required|exists:delivery_options,id',
            'promo_code' => 'exists:promo_codes,code',
            'payment_type' => 'required|numeric',
            'luxury_packagings_id' => 'nullable|exists:luxury_packagings,id',
            'gift_cards_id' => 'nullable|exists:gift_cards,id',
            'address' => 'required|max:255',
            'payment_attachment' => 'required_if:payment_type,2|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'items' => 'required|array|min:1',
            'items.*.product_id' => 'required|exists:products,id',
            'items.*.*.text_design_id' => 'nullable|exists:text_designs,id',
            'items.*.*.sender_name' => 'nullable|string',
            'items.*.*.receiver_name' => 'nullable|string',
            'items.*.*.description' => 'nullable|string',
            'items.*.*.attachment.*' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'items.*.quantity' => 'required|numeric',
        ];
    }

    public function attributes()
    {
        return Master::NiceNames('Order');
    }

    public function persist()
    {

        $Order = new Order();
        $Order->delivery_option_id = $this->delivery_option_id;
        $promo = null;
        if ($this->promo_code) {
            $promo_code = PromoCode::where('code', $this->promo_code)->first();
            if ($promo_code) {
                if ($promo_code->expire_date > Carbon::now()) {
                    $promo = $promo_code->id;
                }
            }
        }

        $Order->promo_code_id = $promo;
        $Order->luxury_packagings_id = $this->luxury_packagings_id;
        $Order->gift_cards_id = $this->gift_cards_id;
        $Order->address = $this->address;
        $Order->payment_type = $this->payment_type;
        $Order->payment_attachment = ($this->payment_attachment) ? $this->payment_attachment : null;
        $Order->user_id = auth()->user()->id;
        $Order->save();

        foreach ($this->items as $ky => $item) {

            $Product = Product::where('id', $item['product_id'])->first();
            if ($Product) {
                $OrderItem = new OrderItem();
                $OrderItem->product_id = $item['product_id'];
                $OrderItem->order_id = $Order->id;
                $OrderItem->quantity = $item['quantity'];
                $OrderItem->save();
            }

            for ($index = 0; $index <= $item['quantity']; $index++) {

                if (isset($item[$index])) {
                    $OrderItemDetails = new OrderItemDetails();
                    if (isset($item[$index]['text_design_id']))
                        $OrderItemDetails->text_design_id = $item[$index]['text_design_id'];
                    if (isset($item[$index]['description']))
                        $OrderItemDetails->description = $item[$index]['description'];


                    if (isset($item[$index]['receiver_name']))
                        $OrderItemDetails->receiver_name = $item[$index]['receiver_name'];
                    if (isset($item[$index]['sender_name']))
                        $OrderItemDetails->sender_name = $item[$index]['sender_name'];
                    $OrderItemDetails->order_item_id = $OrderItem->id;
                    $OrderItemDetails->save();

                    if (isset($item[$index]['attachment'])) {
                        foreach ($item[$index]['attachment'] as $attachment) {
                            $OrderItemDetailsAttachment = new OrderItemDetailsAttachment();
                            $OrderItemDetailsAttachment->attachment = $attachment;
                            $OrderItemDetailsAttachment->order_item_details_id = $OrderItemDetails->id;
                            $OrderItemDetailsAttachment->save();

                        }
                    }
                }

            }

        }

        if ($this->payment_type == 2) {
            $file = new File();
            $file->image = $this->payment_attachment;
            $file->ref_id = $Order->id;
            $file->type = 2;
            $file->save();
        }
//        dd($this->all());
        return $this->successJsonResponse([__('order.add_successful')], $Order, 'Order');


    }

}
