<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderItemDetailsAttachmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order_item_details', function (Blueprint $table) {
            $table->dropColumn('attachment');
        });
        Schema::create('order_item_details_attachments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('attachment')->nullable();
            $table->unsignedBigInteger('order_item_details_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_item_details_attachments');
    }
}
